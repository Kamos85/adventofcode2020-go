package main

import (
	"testing"
)

func TestProblem1(t *testing.T) {
	testInput := []string{
		"L.LL.LL.LL",
		"LLLLLLL.LL",
		"L.L.L..L..",
		"LLLL.LL.LL",
		"L.LL.LL.LL",
		"L.LLLLL.LL",
		"..L.L.....",
		"LLLLLLLLLL",
		"L.LLLLLL.L",
		"L.LLLLL.LL",
	}

	if result := solve1(testInput); result != 37 {
		t.Error("Solving testInput should result in 37, but got {}", result)
	}
}

func TestProblem2(t *testing.T) {
	testInput := []string{
		"L.LL.LL.LL",
		"LLLLLLL.LL",
		"L.L.L..L..",
		"LLLL.LL.LL",
		"L.LL.LL.LL",
		"L.LLLLL.LL",
		"..L.L.....",
		"LLLLLLLLLL",
		"L.LLLLLL.L",
		"L.LLLLL.LL",
	}

	if result := solve2(testInput); result != 26 {
		t.Error("Solving testInput should result in 26, but got {}", result)
	}
}
