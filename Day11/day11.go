package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

var adjacent = []struct {
	y int
	x int
}{
	{1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1}, {0, 1},
}

var plan [][]byte
var planForUpdating [][]byte
var height int
var width int

func solve1(input []string) int {
	height = len(input)
	width = len(input[0])
	plan = make([][]byte, height)
	planForUpdating = make([][]byte, height)

	for y := 0; y < height; y++ {
		plan[y] = make([]byte, len(input[y]))
		planForUpdating[y] = make([]byte, len(input[y]))
		for x := 0; x < width; x++ {
			plan[y][x] = input[y][x]
		}
	}

	for {
		if !updatePlan(updatePositionAtSolve1) {
			break
		}
	}

	return getOccupiedSeatCount()
}

// returns true if plan updated
func updatePlan(updateFunction func(y, x int) bool) bool {
	copyPlanToPlanForUpdating()

	didUpdate := false
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			didUpdate = updateFunction(y, x) || didUpdate
		}
	}
	return didUpdate
}

func copyPlanToPlanForUpdating() {
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			planForUpdating[y][x] = plan[y][x]
		}
	}
}

// returns true if position updated otherwise false
func updatePositionAtSolve1(y, x int) bool {
	if planForUpdating[y][x] == '.' {
		return false // floors don't update
	}
	seatEmpty := planForUpdating[y][x] == 'L'
	adjacentOccupiedSeatCount := getOccupiedAdjacentSeats(y, x)
	if seatEmpty && adjacentOccupiedSeatCount == 0 {
		plan[y][x] = '#'
		return true
	}
	if !seatEmpty && adjacentOccupiedSeatCount >= 4 {
		plan[y][x] = 'L'
		return true
	}
	return false
}

func getOccupiedAdjacentSeats(y, x int) int {
	occupiedSeats := 0
	for _, adj := range adjacent {
		if isPositionOccupied(y+adj.y, x+adj.x) {
			occupiedSeats++
		}
	}
	return occupiedSeats
}

func isPositionOccupied(y, x int) bool {
	if !isPositionOnPlan(y, x) {
		return false
	}
	return planForUpdating[y][x] == '#'
}

func isPositionOnPlan(y, x int) bool {
	if y < 0 || y >= height || x < 0 || x >= width {
		return false
	}
	return true
}

func getOccupiedSeatCount() int {
	totalCount := 0
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			if plan[y][x] == '#' {
				totalCount++
			}
		}
	}
	return totalCount
}

func printPlan() {
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			fmt.Print(string(plan[y][x]))
		}
		fmt.Println()
	}
	fmt.Println()
}

func solve2(input []string) int {
	height = len(input)
	width = len(input[0])
	plan = make([][]byte, height)
	planForUpdating = make([][]byte, height)

	for y := 0; y < height; y++ {
		plan[y] = make([]byte, len(input[y]))
		planForUpdating[y] = make([]byte, len(input[y]))
		for x := 0; x < width; x++ {
			plan[y][x] = input[y][x]
		}
	}

	for {
		if !updatePlan(updatePositionAtSolve2) {
			break
		}
	}

	return getOccupiedSeatCount()
}

// returns true if position updated otherwise false
func updatePositionAtSolve2(y, x int) bool {
	if planForUpdating[y][x] == '.' {
		return false // floors don't update
	}
	seatEmpty := planForUpdating[y][x] == 'L'
	adjacentOccupiedSeatCount := getOccupiedAdjacentSeatsWithLineOfSight(y, x)
	if seatEmpty && adjacentOccupiedSeatCount == 0 {
		plan[y][x] = '#'
		return true
	}
	if !seatEmpty && adjacentOccupiedSeatCount >= 5 {
		plan[y][x] = 'L'
		return true
	}
	return false
}

func getOccupiedAdjacentSeatsWithLineOfSight(y, x int) int {
	occupiedSeats := 0
	for _, adj := range adjacent {
		if seesOccupiedSeatAtPosWithDirection(y, x, adj.y, adj.x) {
			occupiedSeats++
		}
	}
	return occupiedSeats
}

func seesOccupiedSeatAtPosWithDirection(ypos, xpos, ydir, xdir int) bool {
	y := ypos
	x := xpos
	for {
		y += ydir
		x += xdir
		if !isPositionOnPlan(y, x) {
			return false
		}
		if planForUpdating[y][x] != '.' {
			return planForUpdating[y][x] == '#'
		}
	}
}
