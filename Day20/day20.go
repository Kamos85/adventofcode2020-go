package main

import (
	"adventofcode2020-go/utils"
	"fmt"
	"io/ioutil"
	"math"
	"regexp"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

type tile struct {
	num             int
	data            [][]bool
	topSideID       int
	topSideIDRev    int
	bottomSideID    int
	bottomSideIDRev int
	leftSideID      int
	leftSideIDRev   int
	rightSideID     int
	rightSideIDRev  int

	// invariant IDs: same id for rotated or mirrored
	topSideInvariantID    string
	bottomSideInvariantID string
	leftSideInvariantID   string
	rightSideInvariantID  string

	// matches with other tiles
	borderMatches int
	topMatches    *tile
	bottomMatches *tile
	leftMatches   *tile
	rightMatches  *tile
}

func (t *tile) print() {
	for y := 0; y < len(t.data); y++ {
		for x := 0; x < len(t.data); x++ {
			if t.data[y][x] {
				fmt.Print("#")
			} else {
				fmt.Print(".")
			}
		}
		fmt.Println()
	}
}

func (t *tile) getDataOnRow(row int) []bool {
	return t.data[row+1][1 : len(t.data[row+1])-1]
}

func (t *tile) rotateLeft() {
	tempLeftID := t.leftSideID
	tempLeftIDRev := t.leftSideIDRev
	tempLeftInvariantID := t.leftSideInvariantID
	tempLeftMatch := t.leftMatches

	t.leftSideID = t.topSideIDRev
	t.leftSideIDRev = t.topSideID
	t.leftSideInvariantID = t.topSideInvariantID
	t.leftMatches = t.topMatches

	t.topSideID = t.rightSideID
	t.topSideIDRev = t.rightSideIDRev
	t.topSideInvariantID = t.rightSideInvariantID
	t.topMatches = t.rightMatches

	t.rightSideID = t.bottomSideIDRev
	t.rightSideIDRev = t.bottomSideID
	t.rightSideInvariantID = t.bottomSideInvariantID
	t.rightMatches = t.bottomMatches

	t.bottomSideID = tempLeftID
	t.bottomSideIDRev = tempLeftIDRev
	t.bottomSideInvariantID = tempLeftInvariantID
	t.bottomMatches = tempLeftMatch

	data2 := make([][]bool, 0)
	for y := 0; y < len(t.data); y++ {
		row := make([]bool, 0)
		for x := 0; x < len(t.data); x++ {
			row = append(row, t.data[x][len(t.data)-1-y])
		}
		data2 = append(data2, row)
	}
	t.data = data2
}

func (t *tile) mirrorVertical() {
	// swap bottom and top
	tempTopID := t.topSideID
	tempTopIDRev := t.topSideIDRev
	tempInvariantID := t.topSideInvariantID
	tempMatch := t.topMatches
	t.topSideID = t.bottomSideID
	t.topSideIDRev = t.bottomSideIDRev
	t.topSideInvariantID = t.bottomSideInvariantID
	t.topMatches = t.bottomMatches
	t.bottomSideID = tempTopID
	t.bottomSideIDRev = tempTopIDRev
	t.bottomSideInvariantID = tempInvariantID
	t.bottomMatches = tempMatch

	// left and right matches keep the same in a vertical mirror
	// but the ids swap with the rev id
	swap(&t.leftSideID, &t.leftSideIDRev)
	swap(&t.rightSideID, &t.rightSideIDRev)

	data2 := make([][]bool, 0)
	for y := 0; y < len(t.data); y++ {
		data2 = append(data2, t.data[len(t.data)-1-y])
	}
	t.data = data2
}

func swap(a, b *int) {
	t := *a
	*a = *b
	*b = t
}

func createTile(num int, data [][]bool) tile {
	result := tile{num: num, data: data}

	id := 0
	idrev := 0
	for i := len(data[0]) - 1; i >= 0; i-- {
		if data[0][i] {
			id = id | 1<<(len(data[0])-1-i)
		}
		if data[0][i] {
			idrev = idrev | 1<<i
		}
	}
	result.topSideID = id
	result.topSideIDRev = idrev
	if id > idrev {
		result.topSideInvariantID = fmt.Sprint(id) + "-" + fmt.Sprint(idrev)
	} else {
		result.topSideInvariantID = fmt.Sprint(idrev) + "-" + fmt.Sprint(id)
	}

	id = 0
	idrev = 0
	for i := len(data[9]) - 1; i >= 0; i-- {
		if data[9][i] {
			id = id | 1<<(len(data[9])-1-i)
		}
		if data[9][i] {
			idrev = idrev | 1<<i
		}
	}
	result.bottomSideID = id
	result.bottomSideIDRev = idrev
	if id > idrev {
		result.bottomSideInvariantID = fmt.Sprint(id) + "-" + fmt.Sprint(idrev)
	} else {
		result.bottomSideInvariantID = fmt.Sprint(idrev) + "-" + fmt.Sprint(id)
	}

	id = 0
	idrev = 0
	for i := len(data) - 1; i >= 0; i-- {
		if data[i][0] {
			id = id | 1<<(len(data)-1-i)
		}
		if data[i][0] {
			idrev = idrev | 1<<i
		}
	}
	result.leftSideID = id
	result.leftSideIDRev = idrev
	if id > idrev {
		result.leftSideInvariantID = fmt.Sprint(id) + "-" + fmt.Sprint(idrev)
	} else {
		result.leftSideInvariantID = fmt.Sprint(idrev) + "-" + fmt.Sprint(id)
	}

	id = 0
	idrev = 0
	for i := len(data) - 1; i >= 0; i-- {
		if data[i][9] {
			id = id | 1<<(len(data)-1-i)
		}
		if data[i][9] {
			idrev = idrev | 1<<i
		}
	}
	result.rightSideID = id
	result.rightSideIDRev = idrev
	if id > idrev {
		result.rightSideInvariantID = fmt.Sprint(id) + "-" + fmt.Sprint(idrev)
	} else {
		result.rightSideInvariantID = fmt.Sprint(idrev) + "-" + fmt.Sprint(id)
	}

	return result
}

var tiles = make([]tile, 0)

func solve1(input []string) int {
	parseData(input)

	product := 1
	// tiles that have a border match of 2 must be a corner
	for i := 0; i < len(tiles); i++ {
		if tiles[i].borderMatches == 2 {
			product *= tiles[i].num
		}
	}

	return product
}

var parsedData = false

func parseData(input []string) {
	parsedData = true
	tileExp := regexp.MustCompile(`Tile (?P<TILE_NUM>\d+):`)
	i := 0
	for ; i < len(input); i++ {
		matches := utils.GetRegExpMatches(tileExp, input[i])
		tileNum := utils.ToInt(matches["TILE_NUM"])

		data := make([][]bool, 0)
		for j := 0; j < 10; j++ {
			i++
			dataLine := make([]bool, 0)
			for k := 0; k < len(input[i]); k++ {
				dataLine = append(dataLine, string(input[i][k]) == "#")
			}
			data = append(data, dataLine)
		}

		tiles = append(tiles, createTile(tileNum, data))
		i++ // empty line
	}

	for i := 0; i < len(tiles); i++ {
		borderMatches := 0
		for j := 0; j < len(tiles); j++ {
			if i == j {
				continue
			}
			if tiles[i].bottomSideInvariantID == tiles[j].bottomSideInvariantID ||
				tiles[i].bottomSideInvariantID == tiles[j].topSideInvariantID ||
				tiles[i].bottomSideInvariantID == tiles[j].leftSideInvariantID ||
				tiles[i].bottomSideInvariantID == tiles[j].rightSideInvariantID {
				borderMatches++
				tiles[i].bottomMatches = &tiles[j]
			}
			if tiles[i].topSideInvariantID == tiles[j].bottomSideInvariantID ||
				tiles[i].topSideInvariantID == tiles[j].topSideInvariantID ||
				tiles[i].topSideInvariantID == tiles[j].leftSideInvariantID ||
				tiles[i].topSideInvariantID == tiles[j].rightSideInvariantID {
				borderMatches++
				tiles[i].topMatches = &tiles[j]
			}
			if tiles[i].leftSideInvariantID == tiles[j].bottomSideInvariantID ||
				tiles[i].leftSideInvariantID == tiles[j].topSideInvariantID ||
				tiles[i].leftSideInvariantID == tiles[j].leftSideInvariantID ||
				tiles[i].leftSideInvariantID == tiles[j].rightSideInvariantID {
				borderMatches++
				tiles[i].leftMatches = &tiles[j]
			}
			if tiles[i].rightSideInvariantID == tiles[j].bottomSideInvariantID ||
				tiles[i].rightSideInvariantID == tiles[j].topSideInvariantID ||
				tiles[i].rightSideInvariantID == tiles[j].leftSideInvariantID ||
				tiles[i].rightSideInvariantID == tiles[j].rightSideInvariantID {
				borderMatches++
				tiles[i].rightMatches = &tiles[j]
			}
		}

		tiles[i].borderMatches = borderMatches
	}
}

var image [][]bool

func solve2(input []string) int {
	if !parsedData {
		parseData(input)
	}

	// we've figured out that all tiles only fit together in one way, that is
	// each tile side matches exactly one other tile side or none.
	// we can now pick any corner piece and build the puzzle from there

	var cornerTile tile
	for i := 0; i < len(tiles); i++ {
		if tiles[i].borderMatches == 2 {
			cornerTile = tiles[i]
			break
		}
	}

	// rotate such that this piece is a top left corner piece (so has matches on bottom right)
	for {
		if cornerTile.bottomMatches != nil && cornerTile.rightMatches != nil {
			break
		}
		cornerTile.rotateLeft()
	}

	tilemap := make([][]tile, 0)
	row := make([]tile, 0)
	leftTile := &cornerTile
	row = append(row, cornerTile)

	for {
		// build row
		for {
			newTile := leftTile.rightMatches

			rotateLeftCount := 0
			for {
				if newTile.leftMatches != nil && newTile.leftMatches.num == leftTile.num {
					if newTile.leftSideID == leftTile.rightSideID {
						break
					}
				}

				if rotateLeftCount != 3 {
					rotateLeftCount++
					newTile.rotateLeft()
				} else {
					rotateLeftCount = 0
					newTile.mirrorVertical()
				}
			}

			row = append(row, *newTile)

			if newTile.rightMatches == nil {
				break // row complete
			}

			leftTile = newTile
		}
		tilemap = append(tilemap, row)

		if leftTile.bottomMatches == nil {
			break // no bottom, all tiles layed
		}

		// search for tile that matches with the first tile of the row
		topLeftTile := &row[0]
		bottomTile := topLeftTile.bottomMatches

		// orient bottomTile such that bottomTile.topID == topLeftTile.bottomID
		rotateLeftCount := 0
		for {
			if bottomTile.topMatches != nil && bottomTile.topMatches.num == topLeftTile.num {
				if bottomTile.topSideID == topLeftTile.bottomSideID || bottomTile.topSideID == topLeftTile.bottomSideIDRev {
					if bottomTile.rightMatches != nil {
						break
					}
				}
			}
			if rotateLeftCount != 3 {
				rotateLeftCount++
				bottomTile.rotateLeft()
			} else {
				rotateLeftCount = 0
				bottomTile.mirrorVertical()
			}
		}

		leftTile = bottomTile
		row = make([]tile, 0)
		row = append(row, *leftTile)
	}

	// puzzle tiles complete, now combine the tiles in one big image without tile borders
	sideLength := int(math.Sqrt(float64(len(tiles))))
	image = make([][]bool, 0)
	for y := 0; y < sideLength; y++ {
		for i := 0; i < 8; i++ {
			row := make([]bool, 0)
			for x := 0; x < sideLength; x++ {
				data := tilemap[y][x].getDataOnRow(i)
				for j := 0; j < len(data); j++ {
					row = append(row, data[j])
				}
			}
			image = append(image, row)
		}
	}

	// image now holds the complete image, this should have been the end of day 20, but
	// we still have to find the sea monsters
	createSeaMonsterBool()

	rotateLeftCount := 0
	mirrored := false
	seaMonsterCount := 0
	for {
		// DO snake calc
		seaMonsterCount = getSeaMonsterCountInImage()
		if seaMonsterCount != 0 {
			break
		}

		// go through all 8 orientations
		if rotateLeftCount != 3 {
			rotateLeftCount++
			rotateImageLeft()
		} else {
			if mirrored {
				break
			}
			rotateLeftCount = 0
			mirrorImageVertical()
			mirrored = true
		}
	}

	// this assumes sea monsters do not overlap (and they don't seem to cause it's the right answer)
	waterRoughness := countMarksInImage() - seaMonsterCount*marksInSeaMonster

	return waterRoughness
}

var marksInSeaMonster = 15 // amount of '#' in a seamonster
var seaMonster []string = []string{
	"                  # ",
	"#    ##    ##    ###",
	" #  #  #  #  #  #   ",
}
var seaMonsterBool [][]bool

func createSeaMonsterBool() {
	seaMonsterBool = make([][]bool, 0)
	for y := 0; y < len(seaMonster); y++ {
		row := make([]bool, 0)
		for x := 0; x < len(seaMonster[y]); x++ {
			if seaMonster[y][x] == ' ' {
				row = append(row, false)
			} else {
				row = append(row, true)
			}
		}
		seaMonsterBool = append(seaMonsterBool, row)
	}
}

func getSeaMonsterCountInImage() int {
	seaMonsterCount := 0
	// go through every position in the image
	for y := 0; y < len(image)-len(seaMonster); y++ {
		for x := 0; x < len(image[0])-len(seaMonster[0]); x++ {
			// check for sea monster
			noSeaMonster := false
			for yi := 0; yi < len(seaMonster); yi++ {
				for xi := 0; xi < len(seaMonster[0]); xi++ {
					if seaMonsterBool[yi][xi] {
						if !image[y+yi][x+xi] {
							noSeaMonster = true // there can't be a seamonster here, go to next pos in image
							break
						}
					}
				}
				if noSeaMonster {
					break
				}
			}
			if !noSeaMonster {
				seaMonsterCount++
			}
		}
	}

	return seaMonsterCount
}

func rotateImageLeft() {
	image2 := make([][]bool, 0)
	for y := 0; y < len(image); y++ {
		row := make([]bool, 0)
		for x := 0; x < len(image); x++ {
			row = append(row, image[x][len(image)-1-y])
		}
		image2 = append(image2, row)
	}
	image = image2
}

func mirrorImageVertical() {
	image2 := make([][]bool, 0)
	for y := 0; y < len(image); y++ {
		image2 = append(image2, image[len(image)-1-y])
	}
	image = image2
}

func countMarksInImage() int {
	count := 0
	for y := 0; y < len(image); y++ {
		for x := 0; x < len(image[y]); x++ {
			if image[y][x] {
				count++
			}
		}
	}
	return count
}

func printImage() {
	for y := 0; y < len(image); y++ {
		for x := 0; x < len(image[y]); x++ {
			if image[y][x] {
				fmt.Print("#")
			} else {
				fmt.Print(".")
			}
		}
		fmt.Println()
	}
}
