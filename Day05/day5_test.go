package main

import (
	"testing"
)

func TestProblem1(t *testing.T) {
	testInput := []string{
		"FBFBBFFRLR",
		"FFFFFFFLLL",
	}

	if result := solve1(testInput); result != 357 {
		t.Error("Solving testInput should result in 357, but got {}", result)
	}
}

func TestProblem2(t *testing.T) {
	testInput := []string{
		"FFFFFFFLLL", // 0
		//"FFFFFFFLLR", missing seat 1
		"FFFFFFFLRL", // 2
	}

	if result := solve2(testInput); result != 1 {
		t.Error("Solving testInput should result in  1, but got {}", result)
	}
}
