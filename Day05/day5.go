package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func solve1(input []string) int {
	highestSeatID := -1
	for i := 0; i < len(input); i++ {
		seatID := getSeatID(input[i])
		if seatID > highestSeatID {
			highestSeatID = seatID
		}
	}
	return highestSeatID
}

func getSeatID(code string) int {
	codeBinary := strings.ReplaceAll(code, "B", "1")
	codeBinary = strings.ReplaceAll(codeBinary, "F", "0")
	codeBinary = strings.ReplaceAll(codeBinary, "R", "1")
	codeBinary = strings.ReplaceAll(codeBinary, "L", "0")
	rowInt64, _ := strconv.ParseInt(codeBinary, 2, 64)
	return int(rowInt64)
}

func solve2(input []string) int {
	idSet := make(map[int]bool)
	for i := 0; i < len(input); i++ {
		seatID := getSeatID(input[i])
		idSet[seatID] = true
	}

	missingIDsSet := make(map[int]bool)
	lastSeat := getSeatID("BBBBBBBRRR")
	for i := 0; i <= lastSeat; i++ {
		if _, exists := idSet[i]; !exists {
			missingIDsSet[i] = true
		}
	}

	for missingId, _ := range missingIDsSet {
		_, leftSeatExists := idSet[missingId-1]
		_, rightSeatExists := idSet[missingId+1]
		if leftSeatExists && rightSeatExists {
			return missingId
		}
	}

	return -1
}
