package utils

import (
	"regexp"
	"strconv"
)

func GetRegExpMatches(regexp *regexp.Regexp, input string) map[string]string {
	match := regexp.FindStringSubmatch(input)
	result := make(map[string]string)
	if len(match) == 0 {
		return result
	}
	for i, name := range regexp.SubexpNames() {
		if i != 0 && name != "" {
			result[name] = match[i]
		}
	}
	return result
}

func ToInt(str string) int {
	number, err := strconv.Atoi(str)

	if err != nil {
		panic(err)
	}

	return number
}

func ToInt64(str string) int64 {
	number, err := strconv.ParseInt(str, 10, 64)

	if err != nil {
		panic(err)
	}

	return number
}
