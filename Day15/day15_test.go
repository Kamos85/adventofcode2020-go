package main

import (
	"testing"
)

func TestProblem1(t *testing.T) {
	testInput := []string{
		"0,3,6",
	}

	if result := solve1(testInput); result != 436 {
		t.Error("Solving testInput should result in 436, but got {}", result)
	}
}

func TestProblem2(t *testing.T) {
	testInput := []string{
		"0,3,6",
	}

	if result := solve2(testInput); result != 175594 {
		t.Error("Solving testInput should result in 175594, but got {}", result)
	}
}
