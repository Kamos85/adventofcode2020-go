package main

import (
	"adventofcode2020-go/utils"
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

var numbers []int

func solve1(input []string) int {
	numbers = make([]int, 0)
	startNumbersString := strings.Split(input[0], ",")
	for i := 0; i < len(startNumbersString); i++ {
		numbers = append(numbers, utils.ToInt(startNumbersString[i]))
	}

	for {
		nextNumber := getNextNumberSpoken1()
		numbers = append(numbers, nextNumber)
		if len(numbers) == 2020 {
			break
		}
	}

	return numbers[len(numbers)-1]
}

func getNextNumberSpoken1() int {
	lastNumber := numbers[len(numbers)-1]
	for i := len(numbers) - 2; i >= 0; i-- {
		if numbers[i] == lastNumber {
			return len(numbers) - 1 - i
		}
	}
	return 0
}

var lastSpoken map[int]int = make(map[int]int)

func solve2(input []string) int {
	numbers = make([]int, 0)
	startNumbersString := strings.Split(input[0], ",")
	for i := 0; i < len(startNumbersString); i++ {
		readNumber := utils.ToInt(startNumbersString[i])
		numbers = append(numbers, readNumber)
		if i != len(startNumbersString)-1 {
			lastSpoken[readNumber] = i
		}
	}

	for {
		nextNumber := getNextNumberSpoken2()
		numbers = append(numbers, nextNumber)
		if len(numbers) == 30000000 {
			break
		}
	}

	return numbers[len(numbers)-1]
}

func getNextNumberSpoken2() int {
	lastIndex := len(numbers) - 1
	lastNumber := numbers[lastIndex]
	if indexLastSpoken, hasSpokenBefore := lastSpoken[lastNumber]; hasSpokenBefore {
		lastSpoken[lastNumber] = lastIndex
		return lastIndex - indexLastSpoken
	} else {
		lastSpoken[lastNumber] = lastIndex
		return 0
	}
}
