package main

import (
	"adventofcode2020-go/utils"
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func solve1(input []string) int {
	acc, _ := GetTerminatesWithAcc(input, -1)
	return acc
}

func GetTerminatesWithAcc(input []string, switchXthInstruction int) (acc int, terminates bool) {
	executedLinesSet := make(map[int]bool)
	regex := regexp.MustCompile(`(?P<INSTR>nop|acc|jmp) (?P<DATA>[+-]\d+)`)
	i := 0
	acc = 0
	terminates = true
	for {
		if _, hasExecutedLine := executedLinesSet[i]; hasExecutedLine {
			terminates = false
			break
		}
		executedLinesSet[i] = true
		matches := utils.GetRegExpMatches(regex, input[i])
		instr := matches["INSTR"]
		data := utils.ToInt(matches["DATA"])

		if i != switchXthInstruction {
			// normal
			switch instr {
			case "nop":
				i++
			case "acc":
				acc += data
				i++
			case "jmp":
				i += data
			}
		} else {
			// nop and jmp have been switched
			switch instr {
			case "nop":
				i += data
			case "acc":
				acc += data
				i++
			case "jmp":
				i++
			}
		}

		if i >= len(input) {
			break
		}
	}
	return
}

func solve2(input []string) int {
	i := 0
	acc := 0
	terminates := false
	for {
		acc, terminates = GetTerminatesWithAcc(input, i)
		if terminates {
			break
		}
		i++
	}

	return acc
}
