package main

import (
	"testing"
)

func TestProblem1(t *testing.T) {
	testInput := []string{
		"nop +0",
		"acc +1",
		"jmp +4",
		"acc +3",
		"jmp -3",
		"acc -99",
		"acc +1",
		"jmp -4",
		"acc +6",
	}

	if result := solve1(testInput); result != 5 {
		t.Error("Solving testInput should result in 5, but got {}", result)
	}
}

func TestProblem2(t *testing.T) {
	testInput := []string{
		"nop +0",
		"acc +1",
		"jmp +4",
		"acc +3",
		"jmp -3",
		"acc -99",
		"acc +1",
		"jmp -4",
		"acc +6",
	}

	if result := solve2(testInput); result != 8 {
		t.Error("Solving testInput should result in 8, but got {}", result)
	}
}
