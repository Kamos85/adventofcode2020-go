package main

import (
	"testing"
)

func TestProblem1(t *testing.T) {
	testInput := []string{}

	if result := solve1(testInput); result != -1 {
		t.Error("Solving testInput should result in -1, but got {}", result)
	}
}

func TestProblem2(t *testing.T) {
	testInput := []string{}

	if result := solve2(testInput); result != -1 {
		t.Error("Solving testInput should result in -1, but got {}", result)
	}
}
