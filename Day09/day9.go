package main

import (
	"adventofcode2020-go/utils"
	"fmt"
	"io/ioutil"
	"math"
	"strings"
)

var numbers []int64

func main() {
	input := readInput()

	answer1 := solve1(input, 25)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input, 25)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func solve1(input []string, preamble int) int64 {
	numbers = []int64{}
	for i := 0; i < len(input); i++ {
		numbers = append(numbers, utils.ToInt64(input[i]))
	}

	for i := preamble; i < len(input); i++ {
		if !hasSumInPreamble(numbers, preamble, i) {
			return numbers[i]
		}
	}

	return -1
}

func hasSumInPreamble(numbers []int64, preamble int, index int) bool {
	for i := index - preamble; i < index; i++ {
		for j := i; j < index; j++ {
			if numbers[i]+numbers[j] == numbers[index] {
				return true
			}
		}
	}
	return false
}

func solve2(input []string, preamble int) int64 {
	sumTo := solve1(input, preamble)

	lowIndex := 0
	highIndex := 0

	var sumBetweenLowAndHigh int64 = numbers[0]

	for {
		if sumTo == sumBetweenLowAndHigh {
			break
		} else if sumTo > sumBetweenLowAndHigh {
			highIndex++
			sumBetweenLowAndHigh += numbers[highIndex]
		} else { // sumto < sumBetweenLowAndHigh
			sumBetweenLowAndHigh -= numbers[lowIndex]
			lowIndex++
		}
	}

	var lowestValue int64 = math.MaxInt64
	var highestValue int64 = -1
	for i := lowIndex; i <= highIndex; i++ {
		if numbers[i] < lowestValue {
			lowestValue = numbers[i]
		}
		if numbers[i] > highestValue {
			highestValue = numbers[i]
		}
	}

	return lowestValue + highestValue
}
