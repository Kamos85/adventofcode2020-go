package main

import (
	"adventofcode2020-go/utils"
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

type cupNode struct {
	value int
	next  *cupNode
	prev  *cupNode
}

func (c *cupNode) setNextCup(nextCup *cupNode) {
	c.next = nextCup
	nextCup.prev = c
}

func solve1(input []string) string {
	startNode := &cupNode{value: utils.ToInt(string(input[0][0])), next: nil}
	currentNode := startNode
	prevNode := startNode
	for i := 1; i < len(input[0]); i++ {
		currentNode = &cupNode{value: utils.ToInt(string(input[0][i])), next: nil}
		prevNode.setNextCup(currentNode)
		prevNode = currentNode
	}
	currentNode.setNextCup(startNode)

	moves := 0
	currentNode = startNode
	for {
		firstOfSkip3 := currentNode.next
		endOfSkip3 := firstOfSkip3.next.next
		currentNode.setNextCup(endOfSkip3.next)

		targetVal := currentNode.value - 1
		if targetVal == 0 {
			targetVal = 9
		}
		destinationCup := currentNode.next
		for {
			if destinationCup.value == targetVal {
				break
			}
			destinationCup = destinationCup.next
			if destinationCup == currentNode {
				targetVal--
				if targetVal == 0 {
					targetVal = 9
				}
			}
		}

		cupAfterDestination := destinationCup.next
		destinationCup.setNextCup(firstOfSkip3)
		endOfSkip3.setNextCup(cupAfterDestination)
		currentNode = currentNode.next

		moves++
		if moves == 100 {
			break
		}
	}

	// find cup with value 1
	for {
		if currentNode.value == 1 {
			break
		}
		currentNode = currentNode.next
	}

	result := ""
	currentNode = currentNode.next // skip 1
	for {
		result += fmt.Sprint(currentNode.value)
		currentNode = currentNode.next
		if currentNode.value == 1 {
			break
		}
	}

	return result
}

func solve2(input []string) int {
	valueToNode := make(map[int]*cupNode)
	maxVal := 1000000
	startVal := utils.ToInt(string(input[0][0]))
	startNode := &cupNode{value: startVal, next: nil}
	valueToNode[startVal] = startNode
	currentNode := startNode
	prevNode := startNode
	for i := 1; i < len(input[0]); i++ {
		val := utils.ToInt(string(input[0][i]))
		currentNode = &cupNode{value: val, next: nil}
		valueToNode[val] = currentNode
		prevNode.setNextCup(currentNode)
		prevNode = currentNode
	}

	// add cups
	for i := len(input[0]) + 1; i <= maxVal; i++ {
		currentNode = &cupNode{value: i, next: nil}
		valueToNode[i] = currentNode
		prevNode.setNextCup(currentNode)
		prevNode = currentNode
	}

	currentNode.setNextCup(startNode)

	moves := 0
	currentNode = startNode
	for {
		firstOfSkip3 := currentNode.next
		endOfSkip3 := firstOfSkip3.next.next
		currentNode.setNextCup(endOfSkip3.next)

		targetVal := currentNode.value - 1
		if targetVal == 0 {
			targetVal = maxVal
		}

		destinationCup := valueToNode[targetVal]
		for {
			if destinationCup == firstOfSkip3 ||
				destinationCup == firstOfSkip3.next ||
				destinationCup == firstOfSkip3.next.next {
				// destination cup is in the 3 cups we took out, so lower targetval
				targetVal--
				if targetVal == 0 {
					targetVal = maxVal
				}
				destinationCup = valueToNode[targetVal]
				continue
			}
			break
		}

		cupAfterDestination := destinationCup.next
		destinationCup.setNextCup(firstOfSkip3)
		endOfSkip3.setNextCup(cupAfterDestination)
		currentNode = currentNode.next

		moves++
		if moves == 10000000 {
			break
		}
	}

	// find cup with value 1
	currentNode = valueToNode[1]
	firstCupAfterValue1 := currentNode.next.value
	secondCupAfterValue1 := currentNode.next.next.value

	return firstCupAfterValue1 * secondCupAfterValue1
}
