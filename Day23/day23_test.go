package main

import (
	"testing"
)

func TestProblem1(t *testing.T) {
	testInput := []string{
		"389125467",
	}

	if result := solve1(testInput); result != "67384529" {
		t.Error("Solving testInput should result in 67384529, but got {}", result)
	}
}

func TestProblem2(t *testing.T) {
	testInput := []string{
		"389125467",
	}

	if result := solve2(testInput); result != 149245887792 {
		t.Error("Solving testInput should result in 149245887792, but got {}", result)
	}
}
