package main

import (
	"testing"
)

func TestProblem1(t *testing.T) {
	testInput := []int{
		1721,
		979,
		366,
		299,
		675,
		1456,
	}

	if result := solve1(testInput); result != 514579 {
		t.Error("Solving testInput should result in 514579, but got {}", result)
	}
}

func TestProblem2(t *testing.T) {
	testInput := []int{
		1721,
		979,
		366,
		299,
		675,
		1456,
	}

	if result := solve2(testInput); result != 241861950 {
		t.Error("Solving testInput should result in 241861950, but got {}", result)
	}
}
