package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func solve1(input []string) int {
	sum := 0
	groupInfo := make(map[string]bool)
	for i := 0; i < len(input); i++ {
		if input[i] == "" {
			// finish parsing groupInfo
			sum += len(groupInfo)
			// clear groupInfo
			for k := range groupInfo {
				delete(groupInfo, k)
			}
		} else {
			for _, char := range input[i] {
				groupInfo[string(char)] = true
			}
		}
	}
	return sum
}

func solve2(input []string) int {
	allYesAnswers := 0
	groupInfo := make(map[string]int)
	peopleInGroup := 0
	for i := 0; i < len(input); i++ {
		if input[i] == "" {
			// finish parsing groupInfo
			for _, v := range groupInfo {
				if v == peopleInGroup {
					allYesAnswers++
				}
			}

			// clear groupInfo
			for k := range groupInfo {
				delete(groupInfo, k)
			}
			peopleInGroup = 0
		} else {
			peopleInGroup++
			for _, char := range input[i] {
				if _, hasChar := groupInfo[string(char)]; hasChar {
					groupInfo[string(char)] = groupInfo[string(char)] + 1
				} else {
					groupInfo[string(char)] = 1
				}
			}
		}
	}
	return allYesAnswers
}
