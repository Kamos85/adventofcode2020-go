package main

import (
	"testing"
)

func TestProblem1(t *testing.T) {
	testInput := []string{
		"abcx",
		"abcy",
		"abcz",
		"",
	}

	if result := solve1(testInput); result != 6 {
		t.Error("Solving testInput should result in 6, but got {}", result)
	}

	testInput2 := []string{
		"abc",
		"",
		"a",
		"b",
		"c",
		"",
		"ab",
		"ac",
		"",
		"a",
		"a",
		"a",
		"a",
		"",
		"b",
		"",
	}

	if result := solve1(testInput2); result != 11 {
		t.Error("Solving testInput2 should result in 11, but got {}", result)
	}
}

func TestProblem2(t *testing.T) {
	testInput := []string{
		"abc",
		"",
		"a",
		"b",
		"c",
		"",
		"ab",
		"ac",
		"",
		"a",
		"a",
		"a",
		"a",
		"",
		"b",
		"",
	}

	if result := solve2(testInput); result != 6 {
		t.Error("Solving testInput should result in 6, but got {}", result)
	}
}
