package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func solve1(input []string) int {
	blackTiles := make(map[string]bool)
	x := 0 // W/E
	y := 0 // NW/SW
	z := 0 // NE/SE -> NE=1x+1y (NW+E)
	for i := 0; i < len(input); i++ {
		x = 0
		y = 0
		z = 0
		j := 0
		for ; j < len(input[i]); j++ {
			if input[i][j] == 'e' {
				x++
			} else if input[i][j] == 'w' {
				x--
			} else if input[i][j] == 'n' {
				j++
				if input[i][j] == 'w' {
					y++
				} else { // input[i][j] == 'e'
					z++
				}
			} else { // input[i][j] == 's'
				j++
				if input[i][j] == 'w' {
					z--
				} else { // input[i][j] == 'e'
					y--
				}
			}
		}
		x += z
		y += z
		key := strconv.Itoa(x) + "-" + strconv.Itoa(y)
		if _, contained := blackTiles[key]; contained {
			delete(blackTiles, key)
		} else {
			blackTiles[key] = true
		}
	}

	return len(blackTiles)
}

var blackTiles2 map[int]map[int]bool = make(map[int]map[int]bool)
var blackTiles2copy map[int]map[int]bool

func solve2(input []string) int {
	x := 0 // W/E
	y := 0 // NW/SW
	z := 0 // NE/SE -> NE=1x+1y (NW+E)
	for i := 0; i < len(input); i++ {
		x = 0
		y = 0
		z = 0
		j := 0
		for ; j < len(input[i]); j++ {
			if input[i][j] == 'e' {
				x++
			} else if input[i][j] == 'w' {
				x--
			} else if input[i][j] == 'n' {
				j++
				if input[i][j] == 'w' {
					y++
				} else { // input[i][j] == 'e'
					z++
				}
			} else { // input[i][j] == 's'
				j++
				if input[i][j] == 'w' {
					z--
				} else { // input[i][j] == 'e'
					y--
				}
			}
		}
		x += z
		y += z

		flip(x, y)
	}

	for day := 0; day < 100; day++ {
		copyBoard()
		evolveBoard()
	}

	sum := 0
	for _, v := range blackTiles2 {
		sum += len(v)
	}
	return sum
}

func flip(x, y int) {
	if _, xcontained := blackTiles2[x]; xcontained {
		if _, ycontained := blackTiles2[x][y]; ycontained {
			delete(blackTiles2[x], y)
		} else {
			blackTiles2[x][y] = true
		}
	} else {
		blackTiles2[x] = make(map[int]bool)
		blackTiles2[x][y] = true
	}
}

func copyBoard() {
	blackTiles2copy = make(map[int]map[int]bool)
	for x, v := range blackTiles2 {
		blackTiles2copy[x] = map[int]bool{}
		for y, b := range v {
			blackTiles2copy[x][y] = b
		}
	}
}

func evolveBoard() {
	minx := 99999
	maxx := -99999
	miny := 99999
	maxy := -99999
	for x, v := range blackTiles2 {
		if x < minx {
			minx = x
		}
		if x > maxx {
			maxx = x
		}
		for y, _ := range v {
			if y < miny {
				miny = y
			}
			if y > maxy {
				maxy = y
			}
		}
	}

	// add border
	minx--
	maxx++
	miny--
	maxy++

	for x := minx; x <= maxx; x++ {
		for y := miny; y <= maxy; y++ {
			tileIsBlack := false
			if _, xcontained := blackTiles2copy[x]; xcontained {
				if _, ycontained := blackTiles2copy[x][y]; ycontained {
					tileIsBlack = true
				}
			}

			blackCount := getBlackNeighbourCount(x, y)
			if tileIsBlack {
				if blackCount == 0 || blackCount > 2 {
					flip(x, y)
				}
			} else { // tile is white
				if blackCount == 2 {
					flip(x, y)
				}
			}
		}
	}
}

var xneighbour []int = []int{0, 1, 1, 0, -1, -1}
var yneighbour []int = []int{1, 1, 0, -1, -1, 0}

func getBlackNeighbourCount(x, y int) int {
	count := 0
	for i := 0; i < 6; i++ {
		nx := x + xneighbour[i]
		ny := y + yneighbour[i]
		if _, xcontained := blackTiles2copy[nx]; xcontained {
			if _, ycontained := blackTiles2copy[nx][ny]; ycontained {
				count++
			}
		}
	}
	return count
}
