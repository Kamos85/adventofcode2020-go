package main

import (
	"adventofcode2020-go/utils"
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"
)

type validator struct {
	range1Min int
	range1Max int
	range2Min int
	range2Max int
}

func (v validator) String() string {
	return "[" + fmt.Sprint(v.range1Min) + "-" + fmt.Sprint(v.range1Max) + "], [" + fmt.Sprint(v.range2Min) + "-" + fmt.Sprint(v.range2Max) + "]"
}

func (v validator) isValid(number int) bool {
	if v.range1Min <= number && number <= v.range1Max {
		return true
	}
	if v.range2Min <= number && number <= v.range2Max {
		return true
	}
	return false
}

var validTickets [][]int = make([][]int, 0)
var ourTicket []int = make([]int, 0)
var ranSolve1 = false
var indexToFieldName = make([]string, 0)
var fieldToValidator map[string]validator = make(map[string]validator)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func solve1(input []string) int {
	ranSolve1 = true
	regex := regexp.MustCompile(`(?P<FIELD>.*): (?P<RANGE1_MIN>\d+)-(?P<RANGE1_MAX>\d+) or (?P<RANGE2_MIN>\d+)-(?P<RANGE2_MAX>\d+)`)
	i := 0
	for ; i < len(input); i++ {
		if input[i] == "" {
			break
		}
		matches := utils.GetRegExpMatches(regex, input[i])
		field := matches["FIELD"]
		range1Min := utils.ToInt(matches["RANGE1_MIN"])
		range1Max := utils.ToInt(matches["RANGE1_MAX"])
		range2Min := utils.ToInt(matches["RANGE2_MIN"])
		range2Max := utils.ToInt(matches["RANGE2_MAX"])

		fieldToValidator[field] = validator{range1Min, range1Max, range2Min, range2Max}
		indexToFieldName = append(indexToFieldName, field)
	}

	i++ // empty line
	i++ // your ticket:

	ourTicketString := strings.Split(input[i], ",")
	for j := 0; j < len(ourTicketString); j++ {
		ourTicket = append(ourTicket, utils.ToInt(ourTicketString[j]))
	}
	i++

	i++ // empty line
	i++ // nearby tickets:

	badNumbers := make([]int, 0)
	for ; i < len(input); i++ {
		ticketString := input[i]
		ticketNumbersString := strings.Split(ticketString, ",")

		ticketNumbers := make([]int, 0)

		for j := 0; j < len(ticketNumbersString); j++ {
			ticketNumber := utils.ToInt(ticketNumbersString[j])
			ticketNumbers = append(ticketNumbers, ticketNumber)
		}

		ticketHasBadNumber := false
		for j := 0; j < len(ticketNumbers); j++ {
			couldBeValid := false
			for _, v := range fieldToValidator {
				if v.isValid(ticketNumbers[j]) {
					couldBeValid = true
					break
				}
			}
			if !couldBeValid {
				badNumbers = append(badNumbers, ticketNumbers[j])
				ticketHasBadNumber = true
			}
		}

		if !ticketHasBadNumber {
			validTickets = append(validTickets, ticketNumbers)
		}
	}

	sum := 0
	for i := 0; i < len(badNumbers); i++ {
		sum += badNumbers[i]
	}

	return sum
}

var indexToActualFieldName map[int]string = make(map[int]string)

func solve2(input []string) int {
	if !ranSolve1 { // for testing purposes we need to run solve 1 first
		solve1(input)
	}

	unknownIndexToPossibleFields := make([][]string, 0)

	// build possible mappings
	for unknownFieldIndex := 0; unknownFieldIndex < len(indexToFieldName); unknownFieldIndex++ {
		unknownIndexToPossibleFields = append(unknownIndexToPossibleFields, make([]string, 0))

		// go over fields we know, to check which known fields could be this unknown field
		for fieldIndex := 0; fieldIndex < len(indexToFieldName); fieldIndex++ {
			fieldName := indexToFieldName[fieldIndex]
			fieldValidator := fieldToValidator[fieldName]

			fieldValidForAllTickets := true
			// go over tickets for this specific field
			for ticketIndex := 0; ticketIndex < len(validTickets); ticketIndex++ {
				if !fieldValidator.isValid(validTickets[ticketIndex][unknownFieldIndex]) {
					fieldValidForAllTickets = false
					break
				}
			}
			if fieldValidForAllTickets {
				unknownIndexToPossibleFields[unknownFieldIndex] = append(unknownIndexToPossibleFields[unknownFieldIndex], fieldName)
			}
		}
	}

	// eliminate fields by checking where only 1 mapping is possible and then removing that mapping from others
	for {
		didRemoveField := false
		// find element in unknownIndexToPossibleFields that has only 1 field and remove it from all others
		for index, listOfPossibleFields := range unknownIndexToPossibleFields {

			if len(listOfPossibleFields) == 1 {
				fieldName := listOfPossibleFields[0]
				indexToActualFieldName[index] = fieldName
				// remove this field name from all unknownIndexToPossibleFields
				for index2, listOfPossibleFields2 := range unknownIndexToPossibleFields {
					unknownIndexToPossibleFields[index2] = remove(listOfPossibleFields2, fieldName)
				}
				didRemoveField = true
				break
			}
		}

		// continue doing this until there are no fields to remove, then the map is complete
		if !didRemoveField {
			break
		}
	}

	product := 1
	for keyIndex, fieldName := range indexToActualFieldName {
		if strings.HasPrefix(fieldName, "departure") {
			product *= ourTicket[keyIndex]
			fmt.Println("fieldname ", fieldName, keyIndex)
		}
	}

	return product
}

func remove(slice []string, value string) []string {
	index := indexOf(slice, value)
	if index == -1 {
		return slice
	}
	slice[index] = slice[len(slice)-1]
	return slice[:len(slice)-1]
}

func indexOf(s []string, value string) int {
	for i, v := range s {
		if v == value {
			return i
		}
	}
	return -1
}
