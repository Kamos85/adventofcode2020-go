package main

import (
	"adventofcode2020-go/utils"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func solve1(input []string) int {
	player1cards := make([]int, 0, 50)
	player2cards := make([]int, 0, 50)

	i := 1 // skip "Player 1:" text
	for ; i < len(input); i++ {
		if input[i] == "" {
			break
		}
		player1cards = append(player1cards, utils.ToInt(input[i]))
	}
	i++ // skip empty line
	i++ // skip "Player 2:" text
	for ; i < len(input); i++ {
		player2cards = append(player2cards, utils.ToInt(input[i]))
	}

	for {
		// grab both top cards
		player1TopCard := player1cards[0]
		player2TopCard := player2cards[0]
		player1cards = player1cards[1:]
		player2cards = player2cards[1:]
		if player1TopCard > player2TopCard {
			player1cards = append(player1cards, player1TopCard)
			player1cards = append(player1cards, player2TopCard)
		} else {
			player2cards = append(player2cards, player2TopCard)
			player2cards = append(player2cards, player1TopCard)
		}

		if len(player1cards) == 0 || len(player2cards) == 0 {
			break
		}
	}

	// calculate score
	score := 0
	if len(player1cards) == 0 {
		// player2 won
		for i := len(player2cards) - 1; i >= 0; i-- {
			score += (len(player2cards) - i) * player2cards[i]
		}
	} else {
		// player1 won
		for i := len(player1cards) - 1; i >= 0; i-- {
			score += (len(player1cards) - i) * player1cards[i]
		}
	}

	return score
}

type game struct {
	gameNumber   int
	player1cards []int
	player2cards []int

	// when recursing into a subgame we need remember which cards were in play
	cardByPlayer1 int
	cardByPlayer2 int
}

func solve2(input []string) int {
	gamesStack := make([]*game, 0)

	player1cardsInput := make([]int, 0, 50)
	player2cardsInput := make([]int, 0, 50)

	nextGameNumber := 1

	i := 1 // skip "Player 1:" text
	for ; i < len(input); i++ {
		if input[i] == "" {
			break
		}
		player1cardsInput = append(player1cardsInput, utils.ToInt(input[i]))
	}
	i++ // skip empty line
	i++ // skip "Player 2:" text
	for ; i < len(input); i++ {
		player2cardsInput = append(player2cardsInput, utils.ToInt(input[i]))
	}

	currentGame := &game{gameNumber: nextGameNumber, player1cards: player1cardsInput, player2cards: player2cardsInput}
	nextGameNumber++
	gamesStack = append(gamesStack, currentGame)

	seenGameIDsSet := make(map[string]bool, 0)

	alreadySeenGame := false
	for {
		// buid game ID
		var sb strings.Builder
		sb.WriteString(strconv.Itoa(currentGame.gameNumber))
		sb.WriteString("-")
		for i := 0; i < len(currentGame.player1cards); i++ {
			sb.WriteString(strconv.Itoa(currentGame.player1cards[i]))
		}
		sb.WriteString("-")
		for i := 0; i < len(currentGame.player2cards); i++ {
			sb.WriteString(strconv.Itoa(currentGame.player2cards[i]))
		}
		gameID := sb.String()
		if _, alreadySeenGame = seenGameIDsSet[gameID]; alreadySeenGame {
		} else {
			seenGameIDsSet[gameID] = true
		}

		if !alreadySeenGame {
			// grab both top cards
			player1TopCard := currentGame.player1cards[0]
			player2TopCard := currentGame.player2cards[0]
			currentGame.player1cards = currentGame.player1cards[1:]
			currentGame.player2cards = currentGame.player2cards[1:]
			player1CardsLeft := len(currentGame.player1cards)
			player2CardsLeft := len(currentGame.player2cards)
			if player1TopCard <= player1CardsLeft && player2TopCard <= player2CardsLeft {
				// recurse
				var newGamePlayer1Cards = make([]int, player1TopCard)
				copy(newGamePlayer1Cards, currentGame.player1cards[:player1TopCard])
				var newGamePlayer2Cards = make([]int, player2TopCard)
				copy(newGamePlayer2Cards, currentGame.player2cards[:player2TopCard])
				currentGame.cardByPlayer1 = player1TopCard
				currentGame.cardByPlayer2 = player2TopCard
				currentGame = &game{gameNumber: nextGameNumber, player1cards: newGamePlayer1Cards, player2cards: newGamePlayer2Cards, cardByPlayer1: -1, cardByPlayer2: -1}
				nextGameNumber++
				gamesStack = append(gamesStack, currentGame)
				continue
			} else {

				if player1TopCard > player2TopCard {
					currentGame.player1cards = append(currentGame.player1cards, player1TopCard)
					currentGame.player1cards = append(currentGame.player1cards, player2TopCard)
				} else {
					currentGame.player2cards = append(currentGame.player2cards, player2TopCard)
					currentGame.player2cards = append(currentGame.player2cards, player1TopCard)
				}
			}
		}

		if alreadySeenGame || len(currentGame.player1cards) == 0 || len(currentGame.player2cards) == 0 {
			if len(gamesStack) == 1 {
				// main game is done
				break
			} else {
				// sub game is done, go back to game that started this subgame
				player1Won := len(currentGame.player1cards) > 0 || alreadySeenGame
				gamesStack = gamesStack[0 : len(gamesStack)-1]
				currentGame = gamesStack[len(gamesStack)-1]
				// finish round
				if player1Won {
					currentGame.player1cards = append(currentGame.player1cards, currentGame.cardByPlayer1)
					currentGame.player1cards = append(currentGame.player1cards, currentGame.cardByPlayer2)
				} else {
					currentGame.player2cards = append(currentGame.player2cards, currentGame.cardByPlayer2)
					currentGame.player2cards = append(currentGame.player2cards, currentGame.cardByPlayer1)
				}
			}
		}
	}

	// calculate score
	score := 0
	if len(currentGame.player1cards) == 0 && !alreadySeenGame { // if game was already seen then player 1 wins the game
		// player2 won
		for i := len(currentGame.player2cards) - 1; i >= 0; i-- {
			score += (len(currentGame.player2cards) - i) * currentGame.player2cards[i]
		}
	} else {
		// player1 won
		for i := len(currentGame.player1cards) - 1; i >= 0; i-- {
			score += (len(currentGame.player1cards) - i) * currentGame.player1cards[i]
		}
	}

	return score
}
