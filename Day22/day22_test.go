package main

import (
	"testing"
)

func TestProblem1(t *testing.T) {
	testInput := []string{
		"Player 1:",
		"9",
		"2",
		"6",
		"3",
		"1",
		"",
		"Player 2:",
		"5",
		"8",
		"4",
		"7",
		"10",
	}

	if result := solve1(testInput); result != 306 {
		t.Error("Solving testInput should result in 306, but got {}", result)
	}
}

func TestProblem2(t *testing.T) {
	testInput := []string{
		"Player 1:",
		"9",
		"2",
		"6",
		"3",
		"1",
		"",
		"Player 2:",
		"5",
		"8",
		"4",
		"7",
		"10",
	}

	if result := solve2(testInput); result != 291 {
		t.Error("Solving testInput should result in 291, but got {}", result)
	}
}
