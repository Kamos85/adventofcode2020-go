package main

import (
	"fmt"
	"io/ioutil"
	"sort"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

type product struct {
	ingredients map[string]bool
	allergens   map[string]bool
}

var allUniqueIngredientsSet = make(map[string]bool)
var allUniqueAllergensSet = make(map[string]bool)
var products = make([]product, 0)

var ingredientsToPossibleAllergens = make(map[string](map[string]bool))

var solvedPart1 = false

func solve1(input []string) int {
	for i := 0; i < len(input); i++ {
		ingredients := make(map[string]bool)
		allergens := make(map[string]bool)

		parseAllergens := false
		terms := strings.Split(input[i], " ")
		for j := 0; j < len(terms); j++ {
			if terms[j] == "(contains" {
				parseAllergens = true
				continue
			}
			if !parseAllergens {
				ingredients[terms[j]] = true
				allUniqueIngredientsSet[terms[j]] = true
			} else {
				allergen := terms[j][:len(terms[j])-1]
				allergens[allergen] = true
				allUniqueAllergensSet[allergen] = true
			}
		}
		products = append(products, product{ingredients: ingredients, allergens: allergens})
	}

	//fmt.Println("Unique ingredients:", allUniqueIngredientsSet)
	//fmt.Println("Unique allergens:", allUniqueAllergensSet)

	for ingredient := range allUniqueIngredientsSet {
		copyOfUniqueAllergensSet := make(map[string]bool)
		for k, v := range allUniqueAllergensSet {
			copyOfUniqueAllergensSet[k] = v
		}
		ingredientsToPossibleAllergens[ingredient] = copyOfUniqueAllergensSet
	}

	removePossibleAllergensFromIngredientsNotInProduct()

	// count the ingredients that still have a possible allergen mapping
	ingredientToPossibleAllergenCount := 0
	ingredientsWithoutAllergens := make(map[string]bool)
	for ingredient, possibleAllergens := range ingredientsToPossibleAllergens {
		if len(possibleAllergens) > 0 {
			ingredientToPossibleAllergenCount++
			//fmt.Println(ingredient, possibleAllergens)
		} else {
			ingredientsWithoutAllergens[ingredient] = true
		}
	}

	ingredientsWithoutAllergensCount := 0
	if ingredientToPossibleAllergenCount == len(allUniqueAllergensSet) {
		// All ingredients in ingredientsToPossibleAllergens must be assigned an allergen
		for i := 0; i < len(products); i++ {
			for ingredient := range products[i].ingredients {
				if _, contains := ingredientsWithoutAllergens[ingredient]; contains {
					ingredientsWithoutAllergensCount++
				}
			}
		}
	} else {
		fmt.Println("Some ingredients in ingredientsToPossibleAllergens have no allergens") // different solution needed
	}

	solvedPart1 = true

	return ingredientsWithoutAllergensCount
}

func removePossibleAllergensFromIngredientsNotInProduct() {
	// when we have a product with ingredients a, b, c and allergens x, y
	// then all ingredients not in that product cannot have allergens x or y
	for i := 0; i < len(products); i++ {
		for ingredient := range allUniqueIngredientsSet {
			if _, contains := products[i].ingredients[ingredient]; contains {
			} else {
				// this ingredient is not in the product ingredient list and so cannot have one of the listed allergens
				for allergen := range products[i].allergens {
					delete(ingredientsToPossibleAllergens[ingredient], allergen)
				}
			}
		}
	}
}

type ingredientToAllergen struct {
	ingredient string
	allergen   string
}

func solve2(input []string) string {
	if !solvedPart1 {
		solve1(input)
	}

	ingredientWithAllergen := make([]ingredientToAllergen, 0)

	for {
		foundAllergen := false
		for ingredient, possibleAllergens := range ingredientsToPossibleAllergens {
			if len(possibleAllergens) == 1 {
				foundAllergen = true
				allergen := ""
				for i := range possibleAllergens { // loop just to collect the allergen from the map
					allergen = i
				}

				ingredientWithAllergen = append(ingredientWithAllergen, ingredientToAllergen{ingredient: ingredient, allergen: allergen})

				// ingredient has allergen, remove this allergen from all other possible ingredients
				for _, possibleAllergens2 := range ingredientsToPossibleAllergens {
					delete(possibleAllergens2, allergen)
				}
			}
		}
		if !foundAllergen {
			break // processed all
		}
	}

	sort.SliceStable(ingredientWithAllergen, func(i, j int) bool {
		return ingredientWithAllergen[i].allergen < ingredientWithAllergen[j].allergen
	})

	result := ""
	for i := 0; i < len(ingredientWithAllergen); i++ {
		result += ingredientWithAllergen[i].ingredient
		if i != len(ingredientWithAllergen)-1 {
			result += ","
		}
	}

	return result
}
