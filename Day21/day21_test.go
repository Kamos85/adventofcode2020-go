package main

import (
	"testing"
)

func TestProblem1(t *testing.T) {
	testInput := []string{
		"mxmxvkd kfcds sqjhc nhms (contains dairy, fish)",
		"trh fvjkl sbzzf mxmxvkd (contains dairy)",
		"sqjhc fvjkl (contains soy)",
		"sqjhc mxmxvkd sbzzf (contains fish)",
	}

	// keep a list of known allergen ingredients, remove those from the eval between rules
	// rule 1 => a 1 ingredient list with 1 allergen is clear
	// rule 2 => a 2 ingredient list with 2 allergens is clear, but not which one to which one
	// rule 3 => find overlap in allergens

	if result := solve1(testInput); result != 5 {
		t.Error("Solving testInput should result in 5, but got {}", result)
	}
}

func TestProblem2(t *testing.T) {
	testInput := []string{
		"mxmxvkd kfcds sqjhc nhms (contains dairy, fish)",
		"trh fvjkl sbzzf mxmxvkd (contains dairy)",
		"sqjhc fvjkl (contains soy)",
		"sqjhc mxmxvkd sbzzf (contains fish)",
	}

	if result := solve2(testInput); result != "mxmxvkd,sqjhc,fvjkl" {
		t.Error("Solving testInput should result in mxmxvkd,sqjhc,fvjkl, but got {}", result)
	}
}
