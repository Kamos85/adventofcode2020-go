package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input, 3, 1)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func solve1(input []string, slopeX, slopeY int) int {
	treesSeen := 0
	x := 0
	y := 0
	height := len(input)
	width := len(input[0])
	for {
		x = (x + slopeX) % width
		y += slopeY
		if y >= height {
			break
		}
		if input[y][x] == '#' {
			treesSeen++
		}
	}
	return treesSeen
}

func solve2(input []string) int {
	product := 1

	var slopes = []struct {
		x int
		y int
	}{
		{1, 1},
		{3, 1},
		{5, 1},
		{7, 1},
		{1, 2},
	}

	for _, slope := range slopes {
		product *= solve1(input, slope.x, slope.y)
	}

	return product
}
