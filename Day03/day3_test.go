package main

import (
	"testing"
)

func TestProblem1(t *testing.T) {
	testInput := []string{
		"..##.......",
		"#...#...#..",
		".#....#..#.",
		"..#.#...#.#",
		".#...##..#.",
		"..#.##.....",
		".#.#.#....#",
		".#........#",
		"#.##...#...",
		"#...##....#",
		".#..#...#.#",
	}

	if result := solve1(testInput, 3, 1); result != 7 {
		t.Error("Solving testInput should result in 7, but got {}", result)
	}
}

func TestProblem2(t *testing.T) {
	testInput := []string{
		"..##.......",
		"#...#...#..",
		".#....#..#.",
		"..#.#...#.#",
		".#...##..#.",
		"..#.##.....",
		".#.#.#....#",
		".#........#",
		"#.##...#...",
		"#...##....#",
		".#..#...#.#",
	}

	if result := solve2(testInput); result != 336 {
		t.Error("Solving testInput should result in 336, but got {}", result)
	}
}
