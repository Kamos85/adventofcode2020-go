package main

import (
	"adventofcode2020-go/utils"
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func solve1(input []string) int {
	sum := 0
	for i := 0; i < len(input); i++ {
		sum += solveEquationPart1(input[i])
	}
	return sum
}

const PAREN_OPEN = -3
const PLUS = -2
const MULT = -1

func solveEquationPart1(equation string) int {
	var stack []int = make([]int, 0)

	equation = "(" + equation + ")" // for easier parsing

	for i := 0; i < len(equation); i++ {
		switch equation[i] {
		case ' ':
			continue
		case '(':
			stack = append(stack, PAREN_OPEN)
		case '+':
			stack = append(stack, PLUS)
		case '*':
			stack = append(stack, MULT)
		case ')':
			// go back through stack to find paren open and collapse to single value
			j := len(stack) - 1
			for {
				if stack[j] == PAREN_OPEN {
					acc := 0
					lastOperation := 0
					for k := j + 1; k < len(stack); k++ {
						if stack[k] >= 0 && lastOperation == 0 {
							acc = stack[k]
						} else if stack[k] >= 0 && lastOperation != 0 {
							if lastOperation == PLUS {
								acc += stack[k]
							} else {
								acc *= stack[k]
							}
						} else if stack[k] < 0 {
							lastOperation = stack[k]
						}
					}
					stack = stack[:j]
					stack = append(stack, acc)
					break
				}
				j--
			}
		default:
			stack = append(stack, utils.ToInt(string(equation[i])))
		}
	}

	return stack[0]
}

func solve2(input []string) int {
	sum := 0
	for i := 0; i < len(input); i++ {
		sum += solveEquationPart2(input[i])
	}
	return sum
}

func solveEquationPart2(equation string) int {
	var stack []int = make([]int, 0)

	equation = "(" + equation + ")" // for easier parsing

	for i := 0; i < len(equation); i++ {
		switch equation[i] {
		case ' ':
			continue
		case '(':
			stack = append(stack, PAREN_OPEN)
		case '+':
			stack = append(stack, PLUS)
		case '*':
			stack = append(stack, MULT)
		case ')':
			// go back through stack to find paren open and collapse to single value
			j := len(stack) - 1
			for {
				if stack[j] == PAREN_OPEN {
					result := solveEquationNoParentheses(stack[j+1:])
					stack = stack[:j]
					stack = append(stack, result)
					break
				}
				j--
			}
		default:
			stack = append(stack, utils.ToInt(string(equation[i])))
		}
	}

	return stack[0]
}

func solveEquationNoParentheses(stack []int) int {
	acc := 0
	lastOperation := 0
	multStack := make([]int, 0)
	for i := 0; i < len(stack); i++ {
		if stack[i] >= 0 {
			if lastOperation == 0 {
				acc = stack[i]
			} else if lastOperation == PLUS {
				acc += stack[i]
			} else {
				multStack = append(multStack, acc)
				acc = stack[i]
			}
		} else {
			lastOperation = stack[i]
		}
	}
	multStack = append(multStack, acc)

	product := 1
	for i := 0; i < len(multStack); i++ {
		product *= multStack[i]
	}
	return product
}
