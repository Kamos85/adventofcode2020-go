package main

import (
	"testing"
)

func TestProblem1(t *testing.T) {
	testInput := []string{
		//"mask = 000000000000000000000000000000000000",
		"mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X",
		"mem[8] = 11",
		"mem[7] = 101",
		"mem[8] = 0",
	}

	if result := solve1(testInput); result != 165 {
		t.Error("Solving testInput should result in 165, but got {}", result)
	}
}

func TestProblem2(t *testing.T) {
	testInput := []string{
		"mask = 000000000000000000000000000000X1001X",
		"mem[42] = 100",
		"mask = 00000000000000000000000000000000X0XX",
		"mem[26] = 1",
	}

	if result := solve2(testInput); result != 208 {
		t.Error("Solving testInput should result in 208, but got {}", result)
	}
}
