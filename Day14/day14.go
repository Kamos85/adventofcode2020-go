package main

import (
	"adventofcode2020-go/utils"
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func solve1(input []string) int64 {
	memory := make(map[int]int64)
	var setMask int64 = 0
	var resetMask int64 = 0

	maskRegex := regexp.MustCompile(`mask = (?P<MASK>.*)`)
	memRegex := regexp.MustCompile(`mem\[(?P<ADDRESS>\d+)\] = (?P<VALUE>\d+)`)

	for i := 0; i < len(input); i++ {
		if input[i][0:3] == "mas" {
			setMask = 0
			resetMask = 0
			matches := utils.GetRegExpMatches(maskRegex, input[i])
			mask := matches["MASK"]
			for i := len(mask) - 1; i >= 0; i-- {
				if mask[i] == '1' {
					setMask = setMask | (1 << (len(mask) - 1 - i))
				}
				if mask[i] != '0' {
					resetMask = resetMask | (1 << (len(mask) - 1 - i))
				}
			}
		} else { // "mem"
			matches := utils.GetRegExpMatches(memRegex, input[i])
			address := utils.ToInt(matches["ADDRESS"])
			var value int64 = utils.ToInt64(matches["VALUE"])
			value = value | setMask
			value = value & resetMask
			memory[address] = value
		}
	}

	var sum int64 = 0
	for _, v := range memory {
		sum += v
	}

	return sum
}

func solve2(input []string) int64 {
	memory := make(map[string]int64)
	mask := "0"

	maskRegex := regexp.MustCompile(`mask = (?P<MASK>.*)`)
	memRegex := regexp.MustCompile(`mem\[(?P<ADDRESS>\d+)\] = (?P<VALUE>\d+)`)

	for i := 0; i < len(input); i++ {
		if input[i][0:3] == "mas" {
			matches := utils.GetRegExpMatches(maskRegex, input[i])
			mask = matches["MASK"]
		} else { // "mem"
			matches := utils.GetRegExpMatches(memRegex, input[i])
			address := utils.ToInt64(matches["ADDRESS"])
			var value int64 = utils.ToInt64(matches["VALUE"])
			addressAsBinaryString := fmt.Sprintf("%036s", strconv.FormatInt(address, 2))
			maskedAddressBinaryString := createMaskedBinaryString(addressAsBinaryString, mask)

			wildcardCount := strings.Count(maskedAddressBinaryString, "X")
			maxValue := int64(Power(2, wildcardCount))

			var pv int64 = 0
			for pv = 0; pv < maxValue; pv++ { // pv == possible value
				pvAsString := fmt.Sprintf("%0"+fmt.Sprint(wildcardCount)+"s", strconv.FormatInt(pv, 2))
				finalAddress := createAddressWithValues(maskedAddressBinaryString, pvAsString)
				memory[finalAddress] = value
			}
		}
	}

	var sum int64 = 0
	for _, v := range memory {
		sum += v
	}

	return sum
}

func createMaskedBinaryString(addressString, mask string) string {
	var sb strings.Builder
	for i := 0; i < 36; i++ {
		if mask[i] != '0' {
			if mask[i] == '1' {
				sb.WriteByte('1')
			} else { // X
				sb.WriteByte('X')
			}
		} else {
			sb.WriteByte(addressString[i])
		}
	}
	return sb.String()
}

func createAddressWithValues(maskedAddressBinaryString, xValues string) string {
	var sb strings.Builder
	xIndex := 0
	for i := 0; i < 36; i++ {
		if maskedAddressBinaryString[i] == 'X' {
			sb.WriteByte(xValues[xIndex])
			xIndex++
		} else {
			sb.WriteByte(maskedAddressBinaryString[i])
		}
	}
	return sb.String()
}

func Power(a, n int) int {
	var i, result int
	result = 1
	for i = 0; i < n; i++ {
		result *= a
	}
	return result
}
