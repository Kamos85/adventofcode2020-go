package main

import (
	"adventofcode2020-go/utils"
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func solve1(input []string) int {
	value := 1
	subjectNumber := 7
	loopsize := 0
	cardPublicKey := utils.ToInt(input[0])
	doorPublicKey := utils.ToInt(input[1])
	for {
		loopsize++
		value *= subjectNumber
		value = value % 20201227
		if value == cardPublicKey {
			break
		}
	}

	value = 1
	subjectNumber = doorPublicKey
	for i := 0; i < loopsize; i++ {
		value *= subjectNumber
		value = value % 20201227
	}

	return value
}

func solve2(input []string) int {
	return -1
}
