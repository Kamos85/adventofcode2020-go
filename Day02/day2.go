package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func getRegExpMatches(regexp *regexp.Regexp, input string) map[string]string {
	match := regexp.FindStringSubmatch(input)
	result := make(map[string]string)
	for i, name := range regexp.SubexpNames() {
		if i != 0 && name != "" {
			result[name] = match[i]
		}
	}
	return result
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func solve1(input []string) int {
	// build regular expression
	// Example: 1-4 n: nnnnn
	// \d+-\d+ \c: \c+

	validPasswordCount := 0
	myExp := regexp.MustCompile(`(?P<min>\d+)-(?P<max>\d+) (?P<char>[a-z]+): (?P<password>[a-z]+)`)
	for i := 0; i < len(input); i++ {
		matches := getRegExpMatches(myExp, input[i])
		min, _ := strconv.ParseInt(matches["min"], 10, 64)
		max, _ := strconv.ParseInt(matches["max"], 10, 64)
		char := matches["char"]
		password := matches["password"]
		charCount := int64(strings.Count(password, char))
		if min <= charCount && charCount <= max {
			validPasswordCount++
		}
	}

	return validPasswordCount
}

func solve2(input []string) int {
	validPasswordCount := 0
	myExp := regexp.MustCompile(`(?P<pos1>\d+)-(?P<pos2>\d+) (?P<char>[a-z]+): (?P<password>[a-z]+)`)
	for i := 0; i < len(input); i++ {
		matches := getRegExpMatches(myExp, input[i])
		pos1, _ := strconv.ParseInt(matches["pos1"], 10, 64)
		pos2, _ := strconv.ParseInt(matches["pos2"], 10, 64)
		char := matches["char"][0]
		password := matches["password"]
		charPos1 := password[pos1-1]
		charPos2 := password[pos2-1]
		if (char == charPos1) != (char == charPos2) { // xor: either value must be true (but not both)
			validPasswordCount++
		}
	}

	return validPasswordCount
}
