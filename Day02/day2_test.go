package main

import (
	"testing"
)

func TestProblem1(t *testing.T) {
	testInput := []string{
		"1-3 a: abcde",     // valid
		"1-3 b: cdefg",     // invalid
		"2-9 c: ccccccccc", // valid
	}

	if result := solve1(testInput); result != 2 {
		t.Error("Solving testInput should result in 2, but got {}", result)
	}
}

func TestProblem2(t *testing.T) {
	testInput := []string{
		"1-3 a: abcde",     // valid
		"1-3 b: cdefg",     // invalid
		"2-9 c: ccccccccc", // invalid
	}

	if result := solve2(testInput); result != 1 {
		t.Error("Solving testInput should result in 1, but got {}", result)
	}
}
