package main

import (
	"adventofcode2020-go/utils"
	"fmt"
	"io/ioutil"
	"math"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func solve1(input []string) int {
	departTime := utils.ToInt(input[0])
	busLinesText := strings.Split(input[1], ",")
	var busLines []int = make([]int, 0)
	for i := 0; i < len(busLinesText); i++ {
		if busLinesText[i] != "x" {
			busLines = append(busLines, utils.ToInt(busLinesText[i]))
		}
	}

	earliestCatchableBusID := -1
	minimumMinutestoWait := math.MaxInt32
	for i := 0; i < len(busLines); i++ {
		minutesToWait := busLines[i] - departTime%busLines[i]

		if minutesToWait == busLines[i] {
			minutesToWait = 0 // in this case 'departTime % busLines[i]' would be 0
		}
		if minutesToWait < minimumMinutestoWait {
			minimumMinutestoWait = minutesToWait
			earliestCatchableBusID = busLines[i]
		}
	}

	return earliestCatchableBusID * minimumMinutestoWait
}

func solve2(input []string) int {
	busLinesText := strings.Split(input[1], ",")
	var busLines []int = make([]int, 0)
	for i := 0; i < len(busLinesText); i++ {
		if busLinesText[i] != "x" {
			busLines = append(busLines, utils.ToInt(busLinesText[i]))
		} else {
			busLines = append(busLines, -1)
		}
	}

	t := 0
	busNum := 1
	step := busLines[0]
	for {
		if busLines[busNum] == -1 {
			// don't care, can skip this one
			busNum++
			if busNum >= len(busLines) {
				break
			}
			continue
		}

		t += step
		if ((t + busNum) % busLines[busNum]) == 0 {
			step *= busLines[busNum]
			busNum++
			if busNum >= len(busLines) {
				break
			}
		}
	}

	return t
}
