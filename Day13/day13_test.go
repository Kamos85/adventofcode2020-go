package main

import (
	"testing"
)

func TestProblem1(t *testing.T) {
	testInput := []string{
		"939",
		"7,13,x,x,59,x,31,19",
	}

	if result := solve1(testInput); result != 295 {
		t.Error("Solving testInput should result in 295, but got {}", result)
	}
}

func TestProblem2(t *testing.T) {

	testInput := []string{
		"939",
		"1789,37,47,1889",
	}

	if result := solve2(testInput); result != 208 {
		t.Error("Solving testInput should result in 208, but got {}", result)
	}
}
