# AdventOfCode2020-Go

This repository implements solutions in the "Go" language to problems as given by Advent of Code 2020: https://adventofcode.com/2020
