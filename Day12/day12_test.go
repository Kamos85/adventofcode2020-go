package main

import (
	"testing"
)

func TestProblem1(t *testing.T) {
	testInput := []string{
		"F10",
		"N3",
		"F7",
		"R90",
		"F11",
	}

	if result := solve1(testInput); result != 25 {
		t.Error("Solving testInput should result in 25, but got {}", result)
	}
}

func TestProblem2(t *testing.T) {
	testInput := []string{
		"F10",
		"N3",
		"F7",
		"R90",
		"F11",
	}

	if result := solve2(testInput); result != 286 {
		t.Error("Solving testInput should result in 286, but got {}", result)
	}
}
