package main

import (
	"adventofcode2020-go/utils"
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

type Direction int

const (
	North Direction = iota
	East
	South
	West
)

func (d Direction) String() string {
	return [...]string{"North", "East", "South", "West"}[d]
}

func (d Direction) TurnLeft() Direction {
	return [...]Direction{West, North, East, South}[d]
}

func (d Direction) TurnRight() Direction {
	return [...]Direction{East, South, West, North}[d]
}

func (d Direction) X() int {
	return [...]int{0, 1, 0, -1}[d]
}

func (d Direction) Y() int {
	return [...]int{-1, 0, 1, 0}[d]
}

func solve1(input []string) int {
	shipx := 0
	shipy := 0
	shipdir := East

	regex := regexp.MustCompile(`(?P<INSTR>.)(?P<NUMBER>\d+)`)
	for i := 0; i < len(input); i++ {
		matches := utils.GetRegExpMatches(regex, input[i])
		instr := matches["INSTR"]
		number := utils.ToInt(matches["NUMBER"])
		//println(instr, number)

		switch {
		case instr == "N":
			shipy -= number
		case instr == "S":
			shipy += number
		case instr == "E":
			shipx += number
		case instr == "W":
			shipx -= number
		case instr == "L":
			if number == 90 {
				shipdir = shipdir.TurnLeft()
			} else if number == 180 {
				shipdir = shipdir.TurnLeft()
				shipdir = shipdir.TurnLeft()
			} else { // 270
				shipdir = shipdir.TurnRight()
			}
		case instr == "R":
			if number == 90 {
				shipdir = shipdir.TurnRight()
			} else if number == 180 {
				shipdir = shipdir.TurnRight()
				shipdir = shipdir.TurnRight()
			} else { // 270
				shipdir = shipdir.TurnLeft()
			}
		case instr == "F":
			shipx += shipdir.X() * number
			shipy += shipdir.Y() * number
		}
	}

	return Abs(shipx) + Abs(shipy)
}

func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func solve2(input []string) int {
	shipx := 0
	shipy := 0
	waypointrx := 10
	waypointry := -1

	regex := regexp.MustCompile(`(?P<INSTR>.)(?P<NUMBER>\d+)`)
	for i := 0; i < len(input); i++ {
		matches := utils.GetRegExpMatches(regex, input[i])
		instr := matches["INSTR"]
		number := utils.ToInt(matches["NUMBER"])

		switch {
		case instr == "N":
			waypointry -= number
		case instr == "S":
			waypointry += number
		case instr == "E":
			waypointrx += number
		case instr == "W":
			waypointrx -= number
		case instr == "L":
			if number == 90 {
				// 5,1 S  => LEFT => -1,5 E  (y,x) => (-x,y)
				tempx := waypointrx
				waypointrx = waypointry
				waypointry = -tempx
			} else if number == 180 {
				waypointrx = -waypointrx
				waypointry = -waypointry
			} else { // 270
				// 5,1 S => RIGHT => 1,-5 W  (y,x) => (x,-y)
				tempx := waypointrx
				waypointrx = -waypointry
				waypointry = tempx
			}
		case instr == "R":
			if number == 90 {
				tempx := waypointrx
				waypointrx = -waypointry
				waypointry = tempx
			} else if number == 180 {
				waypointrx = -waypointrx
				waypointry = -waypointry
			} else { // 270
				tempx := waypointrx
				waypointrx = waypointry
				waypointry = -tempx
			}
		case instr == "F":
			shipx += waypointrx * number
			shipy += waypointry * number
		}

		//fmt.Println(shipy, shipx, waypointrx, waypointry)
	}

	return Abs(shipx) + Abs(shipy)
}
