package main

import (
	"testing"
)

func TestProblem1(t *testing.T) {
	testInput := []string{
		"16",
		"10",
		"15",
		"5",
		"1",
		"11",
		"7",
		"19",
		"6",
		"12",
		"4",
	}

	if result := solve1(testInput); result != 35 {
		t.Error("Solving testInput should result in 35, but got {}", result)
	}

	testInput2 := []string{
		"28",
		"33",
		"18",
		"42",
		"31",
		"14",
		"46",
		"20",
		"48",
		"47",
		"24",
		"23",
		"49",
		"45",
		"19",
		"38",
		"39",
		"11",
		"1",
		"32",
		"25",
		"35",
		"8",
		"17",
		"7",
		"9",
		"4",
		"2",
		"34",
		"10",
		"3",
	}

	if result := solve1(testInput2); result != 220 {
		t.Error("Solving testInput2 should result in 220, but got {}", result)
	}
}

func TestProblem2(t *testing.T) {
	testInput := []string{
		"16",
		"10",
		"15",
		"5",
		"1",
		"11",
		"7",
		"19",
		"6",
		"12",
		"4",
	}

	if result := solve2(testInput); result != 8 {
		t.Error("Solving testInput should result in 8, but got {}", result)
	}

	testInput2 := []string{
		"28",
		"33",
		"18",
		"42",
		"31",
		"14",
		"46",
		"20",
		"48",
		"47",
		"24",
		"23",
		"49",
		"45",
		"19",
		"38",
		"39",
		"11",
		"1",
		"32",
		"25",
		"35",
		"8",
		"17",
		"7",
		"9",
		"4",
		"2",
		"34",
		"10",
		"3",
	}

	if result := solve2(testInput2); result != 19208 {
		t.Error("Solving testInput2 should result in 19208, but got {}", result)
	}
}
