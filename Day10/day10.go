package main

import (
	"adventofcode2020-go/utils"
	"fmt"
	"io/ioutil"
	"math"
	"sort"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func solve1(input []string) int {
	numbers := []int{0}
	for i := 0; i < len(input); i++ {
		numbers = append(numbers, utils.ToInt(input[i]))
	}
	sort.Ints(numbers)
	numbers = append(numbers, numbers[len(numbers)-1]+3) // add internal adapter

	diff1Count := 0
	diff3Count := 0
	for i := 1; i < len(numbers); i++ {
		prev := numbers[i-1]
		if numbers[i]-1 == prev {
			diff1Count++
			continue
		}
		if numbers[i]-3 == prev {
			diff3Count++
		}
	}
	return diff1Count * diff3Count
}

func solve2(input []string) int64 {
	numbers := []int{0}
	for i := 0; i < len(input); i++ {
		numbers = append(numbers, utils.ToInt(input[i]))
	}
	sort.Ints(numbers)
	numbers = append(numbers, numbers[len(numbers)-1]+3) // add internal adapter

	consecutiveNumberLength := 1
	var comboAmount int64 = 1
	for i := 1; i < len(numbers); i++ {
		prev := numbers[i-1]
		if numbers[i]-1 == prev {
			consecutiveNumberLength++
		} else {
			if consecutiveNumberLength > 2 {
				comboAmount = getNewCombo(comboAmount, consecutiveNumberLength)
			}
			consecutiveNumberLength = 1
		}
	}
	if consecutiveNumberLength > 2 {
		comboAmount = getNewCombo(comboAmount, consecutiveNumberLength)
	}

	return comboAmount
}

func getNewCombo(currentCombo int64, consecutiveNumberLength int) int64 {
	if consecutiveNumberLength == 3 {
		return currentCombo * 2
	}
	if consecutiveNumberLength == 4 {
		return currentCombo * 4
	}
	if consecutiveNumberLength == 5 {
		return currentCombo * 7
	}

	fmt.Println("Not calculated how many combinations this adds")
	return 0
}

func powInt64(x, y int64) int64 {
	return int64(math.Pow(float64(x), float64(y)))
}
