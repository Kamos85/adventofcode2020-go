package main

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func solve1(input []string) int {
	validPassportCount := 0

	var passportInfo map[string]string = map[string]string{}

	for i := 0; i < len(input); i++ {
		if input[i] == "" {
			// finish parsing password info, check validity
			_, hasByr := passportInfo["byr"]
			_, hasIyr := passportInfo["iyr"]
			_, hasEyr := passportInfo["eyr"]
			_, hasHgt := passportInfo["hgt"]
			_, hasHcl := passportInfo["hcl"]
			_, hasEcl := passportInfo["ecl"]
			_, hasPid := passportInfo["pid"]
			if hasByr && hasIyr && hasEyr && hasHgt && hasHcl && hasEcl && hasPid {
				validPassportCount++
			}

			// clear passportInfo
			for k := range passportInfo {
				delete(passportInfo, k)
			}
		} else {
			info := strings.Split(input[i], " ")
			for j := 0; j < len(info); j++ {
				keyvalue := strings.Split(info[j], ":")
				passportInfo[keyvalue[0]] = keyvalue[1]
			}
		}
	}

	return validPassportCount
}

func solve2(input []string) int {
	validPassportCount := 0

	var passportInfo map[string]string = map[string]string{}

	for i := 0; i < len(input); i++ {
		if input[i] == "" {
			// finish parsing password info, check validity
			_, hasByr := passportInfo["byr"]
			_, hasIyr := passportInfo["iyr"]
			_, hasEyr := passportInfo["eyr"]
			_, hasHgt := passportInfo["hgt"]
			_, hasHcl := passportInfo["hcl"]
			_, hasEcl := passportInfo["ecl"]
			_, hasPid := passportInfo["pid"]
			if hasByr && hasIyr && hasEyr && hasHgt && hasHcl && hasEcl && hasPid {
				if isPassportDataValid(passportInfo) {
					validPassportCount++
				}
			}

			// clear passportInfo
			for k := range passportInfo {
				delete(passportInfo, k)
			}
		} else {
			info := strings.Split(input[i], " ")
			for j := 0; j < len(info); j++ {
				keyvalue := strings.Split(info[j], ":")
				passportInfo[keyvalue[0]] = keyvalue[1]
			}
		}
	}

	return validPassportCount
}

func isPassportDataValid(passportInfo map[string]string) bool {
	if len(passportInfo["byr"]) != 4 {
		return false
	}
	byr, _ := strconv.ParseInt(passportInfo["byr"], 10, 64)
	if byr < 1920 || byr > 2002 {
		return false
	}

	if len(passportInfo["iyr"]) != 4 {
		return false
	}
	iyr, _ := strconv.ParseInt(passportInfo["iyr"], 10, 64)
	if iyr < 2010 || iyr > 2020 {
		return false
	}

	if len(passportInfo["eyr"]) != 4 {
		return false
	}
	eyr, _ := strconv.ParseInt(passportInfo["eyr"], 10, 64)
	if eyr < 2020 || eyr > 2030 {
		return false
	}

	hgt := passportInfo["hgt"]
	hgtsuffix := hgt[len(hgt)-2:]
	if hgtsuffix == "in" || hgtsuffix == "cm" {
		if hgtsuffix == "in" {
			in, _ := strconv.ParseInt(hgt[0:len(hgt)-2], 10, 64)
			if in < 59 || in > 76 {
				return false
			}
		}
		if hgtsuffix == "cm" {
			cm, _ := strconv.ParseInt(hgt[0:len(hgt)-2], 10, 64)
			if cm < 150 || cm > 193 {
				return false
			}
		}
	} else {
		return false
	}

	hcl := passportInfo["hcl"]
	if hcl[0] != '#' || len(hcl) != 7 {
		return false
	}
	hclsuffix := hcl[1:]
	matchedhcl, _ := regexp.Match(`[0-9a-f]{6}`, []byte(hclsuffix))
	if !matchedhcl {
		return false
	}

	ecl := passportInfo["ecl"]
	if len(ecl) != 3 {
		return false
	}
	matchedecl, _ := regexp.Match(`amb|blu|brn|gry|grn|hzl|oth`, []byte(ecl))
	if !matchedecl {
		return false
	}

	pid := passportInfo["pid"]
	if len(pid) != 9 {
		return false
	}
	matchedpid, _ := regexp.Match(`[0-9]{9}`, []byte(pid))
	if !matchedpid {
		return false
	}

	return true
}
