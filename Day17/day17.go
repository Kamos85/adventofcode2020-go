package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

func main() {
	input := readInput()

	// answer1 := solve1(input)
	// fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

var field3d map[string]bool = make(map[string]bool)
var field3dCopy map[string]bool = make(map[string]bool)
var minx = 0
var maxx = 0
var miny = 0
var maxy = 0
var minz = 0
var maxz = 0

func solve1(input []string) int {
	// read field3d
	maxy = len(input) - 1
	maxx = len(input[0]) - 1
	for y := 0; y < len(input); y++ {
		for x := 0; x < len(input[y]); x++ {
			if input[y][x] == '#' {
				field3d["0,"+fmt.Sprint(y)+","+fmt.Sprint(x)] = true
			}
		}
	}

	for round := 0; round < 6; round++ {
		copyfield3d()
		doCycle3d()
		//printfield3d()
	}

	return getActiveCountInfield3d()
}

func copyfield3d() {
	field3dCopy = make(map[string]bool)
	for k, v := range field3d {
		field3dCopy[k] = v
	}
}

func doCycle3d() {
	field3d = make(map[string]bool)
	for z := minz - 1; z <= maxz+1; z++ {
		for y := miny - 1; y <= maxy+1; y++ {
			for x := minx - 1; x <= maxx+1; x++ {
				neighbourCount := getNeighbourCount(z, y, x)
				key := fmt.Sprint(z) + "," + fmt.Sprint(y) + "," + fmt.Sprint(x)
				//fmt.Println(key, neighbourCount)
				if _, ok := field3dCopy[key]; ok {
					if neighbourCount == 2 || neighbourCount == 3 {
						field3d[key] = true
					}
				} else {
					if neighbourCount == 3 {
						field3d[key] = true
					}
				}
			}
		}
	}
	minx--
	miny--
	minz--
	maxx++
	maxy++
	maxz++
}

func getNeighbourCount(z, y, x int) int {
	neighbourCount := 0
	for tz := z - 1; tz <= z+1; tz++ {
		for ty := y - 1; ty <= y+1; ty++ {
			for tx := x - 1; tx <= x+1; tx++ {
				if tz == z && ty == y && tx == x {
					continue
				}
				key := fmt.Sprint(tz) + "," + fmt.Sprint(ty) + "," + fmt.Sprint(tx)
				if _, ok := field3dCopy[key]; ok {
					neighbourCount++
				}
			}
		}
	}
	return neighbourCount
}

func printfield3d() {
	for z := minz; z <= maxz; z++ {
		fmt.Println(z)
		for y := miny; y <= maxy; y++ {
			for x := minx; x <= maxx; x++ {
				key := fmt.Sprint(z) + "," + fmt.Sprint(y) + "," + fmt.Sprint(x)
				if _, ok := field3d[key]; ok {
					fmt.Print("#")
				} else {
					fmt.Print(".")
				}
			}
			fmt.Println()
		}
		fmt.Println()
	}
}

func getActiveCountInfield3d() int {
	count := 0
	for z := minz; z <= maxz; z++ {
		for y := miny; y <= maxy; y++ {
			for x := minx; x <= maxx; x++ {
				key := fmt.Sprint(z) + "," + fmt.Sprint(y) + "," + fmt.Sprint(x)
				if _, ok := field3d[key]; ok {
					count++
				}
			}
		}
	}
	return count
}

var field4d map[string]bool = make(map[string]bool)
var field4dCopy map[string]bool = make(map[string]bool)
var minw = 0
var maxw = 0

func solve2(input []string) int {
	// read field4d
	maxy = len(input) - 1
	maxx = len(input[0]) - 1
	for y := 0; y < len(input); y++ {
		for x := 0; x < len(input[y]); x++ {
			if input[y][x] == '#' {
				field4d["0,0,"+fmt.Sprint(y)+","+fmt.Sprint(x)] = true
			}
		}
	}

	for round := 0; round < 6; round++ {
		copyfield4d()
		doCycle4d()
		//printfield4d()
	}

	return getActiveCountInfield4d()
}

func copyfield4d() {
	field4dCopy = make(map[string]bool)
	for k, v := range field4d {
		field4dCopy[k] = v
	}
}

func doCycle4d() {
	field4d = make(map[string]bool)
	for w := minw - 1; w <= maxw+1; w++ {
		for z := minz - 1; z <= maxz+1; z++ {
			for y := miny - 1; y <= maxy+1; y++ {
				for x := minx - 1; x <= maxx+1; x++ {
					neighbourCount := getNeighbourCount4d(w, z, y, x)
					key := fmt.Sprint(w) + "," + fmt.Sprint(z) + "," + fmt.Sprint(y) + "," + fmt.Sprint(x)
					//fmt.Println(key, neighbourCount)
					if _, ok := field4dCopy[key]; ok {
						if neighbourCount == 2 || neighbourCount == 3 {
							field4d[key] = true
						}
					} else {
						if neighbourCount == 3 {
							field4d[key] = true
						}
					}
				}
			}
		}
	}
	minx--
	miny--
	minz--
	minw--
	maxx++
	maxy++
	maxz++
	maxw++
}

func getNeighbourCount4d(w, z, y, x int) int {
	neighbourCount := 0
	for tw := w - 1; tw <= w+1; tw++ {
		for tz := z - 1; tz <= z+1; tz++ {
			for ty := y - 1; ty <= y+1; ty++ {
				for tx := x - 1; tx <= x+1; tx++ {
					if tw == w && tz == z && ty == y && tx == x {
						continue
					}
					key := fmt.Sprint(tw) + "," + fmt.Sprint(tz) + "," + fmt.Sprint(ty) + "," + fmt.Sprint(tx)
					if _, ok := field4dCopy[key]; ok {
						neighbourCount++
					}
				}
			}
		}
	}
	return neighbourCount
}

func printfield4d() {
	for w := minw; w <= maxw; w++ {
		for z := minz; z <= maxz; z++ {
			fmt.Println(w, z)
			for y := miny; y <= maxy; y++ {
				for x := minx; x <= maxx; x++ {
					key := fmt.Sprint(z) + "," + fmt.Sprint(y) + "," + fmt.Sprint(x)
					if _, ok := field4d[key]; ok {
						fmt.Print("#")
					} else {
						fmt.Print(".")
					}
				}
				fmt.Println()
			}
			fmt.Println()
		}
	}
}

func getActiveCountInfield4d() int {
	count := 0
	for w := minw; w <= maxw; w++ {
		for z := minz; z <= maxz; z++ {
			for y := miny; y <= maxy; y++ {
				for x := minx; x <= maxx; x++ {
					key := fmt.Sprint(w) + "," + fmt.Sprint(z) + "," + fmt.Sprint(y) + "," + fmt.Sprint(x)
					if _, ok := field4d[key]; ok {
						count++
					}
				}
			}
		}
	}
	return count
}
