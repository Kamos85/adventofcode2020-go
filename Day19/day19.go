package main

import (
	"adventofcode2020-go/utils"
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

const CHAR_KIND = 0
const CONCAT_KIND = 1
const OR_KIND = 2

type validator struct {
	ruleNum     int
	kind        int
	char        string
	concatrule  []int
	subrule1    []int
	subrule2    []int
	isRecursive bool // for part2
}

func (v validator) String() string {
	result := ""
	switch v.kind {
	case CHAR_KIND:
		result = v.char
	case CONCAT_KIND:
		for i := 0; i < len(v.concatrule); i++ {
			result += fmt.Sprint(v.concatrule[i]) + " "
		}
	case OR_KIND:
		for i := 0; i < len(v.subrule1); i++ {
			result += fmt.Sprint(v.subrule1[i]) + " "
		}
		result += "| "
		for i := 0; i < len(v.subrule2); i++ {
			result += fmt.Sprint(v.subrule2[i]) + " "
		}
	}
	return result
}

func (v validator) ToRegex(recurseDepth int) string {
	result := ""
	switch v.kind {
	case CHAR_KIND:
		result = v.char
	case CONCAT_KIND:
		for i := 0; i < len(v.concatrule); i++ {
			result += rules[v.concatrule[i]].ToRegex(6)
		}
	case OR_KIND:
		result += "("
		for i := 0; i < len(v.subrule1); i++ {
			result += rules[v.subrule1[i]].ToRegex(6)
		}

		if v.isRecursive && recurseDepth == 0 {
		} else {
			result += "|"
			for i := 0; i < len(v.subrule2); i++ {
				if v.ruleNum == v.subrule2[i] {
					result += rules[v.subrule2[i]].ToRegex(recurseDepth - 1)
				} else {
					result += rules[v.subrule2[i]].ToRegex(6)
				}
			}
		}
		result += ")"
	}
	return result
}

var rules map[int]validator = map[int]validator{}
var messages []string = make([]string, 0)

func solve1(input []string) int {
	parseInput(input)

	genregexstring := rules[0].ToRegex(6)
	generatedExp := regexp.MustCompile("^" + genregexstring + "$")

	// validate inputs with rules
	validInputCount := 0
	for i := 0; i < len(messages); i++ {
		ok := generatedExp.MatchString(messages[i])
		if ok {
			validInputCount++
		}
	}

	return validInputCount
}

func parseInput(input []string) {
	baseRuleExp := regexp.MustCompile(`(?P<RULE_NUM>\d+): \"(?P<CHAR>.)\"`)                                                                            //13: "b"
	concatRuleExp := regexp.MustCompile(`(?P<RULE_NUM>\d+): (?P<CONCAT_RULE1>\d+)\s?(?P<CONCAT_RULE2>\d+)?\s?(?P<CONCAT_RULE3>\d+)?`)                  //91: 13 7 x
	orRuleExp := regexp.MustCompile(`(?P<RULE_NUM>\d+): (?P<SUB_RULE1_1>\d+)\s?(?P<SUB_RULE1_2>\d+)? \| (?P<SUB_RULE2_1>\d+)\s?(?P<SUB_RULE2_2>\d+)?`) //83: 13 78 | 7 74

	// build rule set
	i := 0
	for ; i < len(input); i++ {
		if input[i] == "" {
			break // rule parsing done
		}

		matches := utils.GetRegExpMatches(orRuleExp, input[i])
		if len(matches) > 0 {
			ruleNum := utils.ToInt(matches["RULE_NUM"])
			subrules1 := make([]int, 0)
			subrules1 = append(subrules1, utils.ToInt(matches["SUB_RULE1_1"]))
			if matches["SUB_RULE1_2"] != "" {
				subrules1 = append(subrules1, utils.ToInt(matches["SUB_RULE1_2"]))
			}
			subrules2 := make([]int, 0)
			subrules2 = append(subrules2, utils.ToInt(matches["SUB_RULE2_1"]))
			if matches["SUB_RULE2_2"] != "" {
				subrules2 = append(subrules2, utils.ToInt(matches["SUB_RULE2_2"]))
			}
			rules[ruleNum] = validator{ruleNum: ruleNum, kind: OR_KIND, subrule1: subrules1, subrule2: subrules2}
			continue
		}
		matches = utils.GetRegExpMatches(concatRuleExp, input[i])
		if len(matches) > 0 {
			ruleNum := utils.ToInt(matches["RULE_NUM"])
			concatRules := make([]int, 0)
			concatRules = append(concatRules, utils.ToInt(matches["CONCAT_RULE1"]))
			if matches["CONCAT_RULE2"] != "" {
				concatRules = append(concatRules, utils.ToInt(matches["CONCAT_RULE2"]))
			}
			if matches["CONCAT_RULE3"] != "" {
				concatRules = append(concatRules, utils.ToInt(matches["CONCAT_RULE3"]))
			}
			rules[ruleNum] = validator{ruleNum: ruleNum, kind: CONCAT_KIND, concatrule: concatRules}
			continue
		}
		matches = utils.GetRegExpMatches(baseRuleExp, input[i])
		if len(matches) > 0 {
			ruleNum := utils.ToInt(matches["RULE_NUM"])
			rules[ruleNum] = validator{ruleNum: ruleNum, kind: CHAR_KIND, char: matches["CHAR"]}
			continue
		}

		fmt.Println("NO RULE MATCH FOUND, error")
		return
	}

	i++ // skip empty line

	for ; i < len(input); i++ {
		messages = append(messages, input[i])
	}
}

func solve2(input []string) int {
	if len(rules) == 0 { // in case of running tests
		parseInput(input)
	}

	// update rules as per part2
	rule8 := rules[8]
	rule8.kind = OR_KIND
	rule8.concatrule = []int{}
	rule8.subrule1 = []int{42}
	rule8.subrule2 = []int{42, 8}
	rule8.isRecursive = true
	rules[8] = rule8

	rule11 := rules[11]
	rule11.kind = OR_KIND
	rule11.concatrule = []int{}
	rule11.subrule1 = []int{42, 31}
	rule11.subrule2 = []int{42, 11, 31}
	rule11.isRecursive = true
	rules[11] = rule11

	genregexstring := rules[0].ToRegex(6)

	generatedExp := regexp.MustCompile("^" + genregexstring + "$")

	// validate inputs with rules
	validInputCount := 0
	for i := 0; i < len(messages); i++ {
		ok := generatedExp.MatchString(messages[i])
		if ok {
			validInputCount++
		}
	}

	return validInputCount
}
