package main

import (
	"adventofcode2020-go/utils"
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"
)

func main() {
	input := readInput()

	answer1 := solve1(input)
	fmt.Println("Problem 1 answer: ", answer1)

	answer2 := solve2(input)
	fmt.Println("Problem 2 answer: ", answer2)
}

func readInput() []string {
	dat, err := ioutil.ReadFile("input.txt")
	check(err)
	lines := strings.Split(string(dat), "\n")
	return lines
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func solve1(input []string) int {
	bags := make(map[string]bagcontainer)
	mainRegex := regexp.MustCompile(`(?P<MAINID1>[a-z]+) (?P<MAINID2>[a-z]+) bags contain (?P<REST>.*)`)

	for i := 0; i < len(input); i++ {
		matches := utils.GetRegExpMatches(mainRegex, input[i])
		MAINID1 := matches["MAINID1"]
		MAINID2 := matches["MAINID2"]
		bags[MAINID1+"_"+MAINID2] = bagcontainer{make(map[string]int)}
		rest := matches["REST"]

		if rest == "no other bags." {
			continue
		}

		// parse bag container
		containerRegex := regexp.MustCompile(`(,|\..)?(?P<NUM>\d+) (?P<ID1>[a-z]+) (?P<ID2>[a-z]+) bag(s?)(?P<REST>.*)`)
		for {
			containerMatches := utils.GetRegExpMatches(containerRegex, rest)
			num := utils.ToInt(containerMatches["NUM"])
			ID1 := containerMatches["ID1"]
			ID2 := containerMatches["ID2"]
			bags[MAINID1+"_"+MAINID2].bagsWithAmount[ID1+"_"+ID2] = num

			rest = containerMatches["REST"]
			if len(rest) < 3 {
				break
			}
		}
	}

	bagsContainingShinyGoldSet := make(map[string]bool)
	bagsContainingShinyGoldSet["shiny_gold"] = true
	prevNumContains := 1

	// for each bag containing the shiny gold bag i, go over all bags
	// and if a bag j contains bag i then add j to the set of bags containing the shiny gold bag
	for {
		for bag := range bagsContainingShinyGoldSet {
			for k, v := range bags {
				if _, hasBag := v.bagsWithAmount[bag]; hasBag {
					bagsContainingShinyGoldSet[k] = true
				}
			}
		}

		// once the set doesn't grow anymore we've found all bags containing the shiny gold bag
		if prevNumContains == len(bagsContainingShinyGoldSet) {
			break
		}
		prevNumContains = len(bagsContainingShinyGoldSet)
	}

	return len(bagsContainingShinyGoldSet) - 1 // -1 cause shiny gold does not contain itself
}

type bagcontainer struct {
	bagsWithAmount map[string]int
}

func solve2(input []string) int {
	bags := make(map[string]bagcontainer)
	mainRegex := regexp.MustCompile(`(?P<MAINID1>[a-z]+) (?P<MAINID2>[a-z]+) bags contain (?P<REST>.*)`)

	for i := 0; i < len(input); i++ {
		matches := utils.GetRegExpMatches(mainRegex, input[i])
		MAINID1 := matches["MAINID1"]
		MAINID2 := matches["MAINID2"]
		bags[MAINID1+"_"+MAINID2] = bagcontainer{make(map[string]int)}
		rest := matches["REST"]

		if rest == "no other bags." {
			continue
		}

		// parse bag container
		containerRegex := regexp.MustCompile(`(,|\..)?(?P<NUM>\d+) (?P<ID1>[a-z]+) (?P<ID2>[a-z]+) bag(s?)(?P<REST>.*)`)
		for {
			containerMatches := utils.GetRegExpMatches(containerRegex, rest)
			num := utils.ToInt(containerMatches["NUM"])
			ID1 := containerMatches["ID1"]
			ID2 := containerMatches["ID2"]
			bags[MAINID1+"_"+MAINID2].bagsWithAmount[ID1+"_"+ID2] = num

			rest = containerMatches["REST"]
			if len(rest) < 3 {
				break
			}
		}
	}

	bagCounter := make(map[string]int)
	bagsInShinyGold := RecurseBags("shiny_gold", bags, bagCounter)

	return bagsInShinyGold
}

func RecurseBags(currentBag string, bags map[string]bagcontainer, bagCounter map[string]int) int {
	if count, hasCounted := bagCounter[currentBag]; hasCounted {
		return count
	}

	sum := 0
	for subBag, amount := range bags[currentBag].bagsWithAmount {
		sum += (RecurseBags(subBag, bags, bagCounter) + 1) * amount // + 1 because the bag holding other bags is also a bag
	}
	bagCounter[currentBag] = sum
	return sum
}
